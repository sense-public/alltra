const controller = {};
const fs = require('fs');
const path = require('path');
const sha256 = require('crypto-js/sha256');
const Base64 = require('crypto-js/enc-base64');
const Utf8 = require('crypto-js/enc-utf8');
const net = require("node:net");
const HOST = `${process.env.LISTEN_HOST}`;
const PORT = parseInt(`${process.env.LISTEN_PORT}`);
const funHistory = require('./account_controllers');

const modifyConfig = (t, c, e, m) =>{
  if(t === "AG1"){
    let C =  c.split(",").map(function(i){ if(i !== "" && typeof i === "string"){ if (i.trim().split("")[i.trim().length-1].includes("/") === false){ if( ! i.includes(":") ) { return i+"/" } else { if (i.trim().split("")[i.trim().length-1].includes("\\")) { return i } else { return i+`\\` } } } else { return i } } else{ return i } }).filter(i => i)
    return C.join(",")
  }else if(t === "AG2"){
    if(parseInt(m) === -1){
      return c+e
    }else{
      let C = []
      if(parseInt(m) === 0){
        C = c.split(",").map(function(i){ if (i[i.length - 1] !== "/" && i !== ""){ if ( ! i.includes(":") ) { return i += `/*${req.body.extension}` } else { return i+= `\\*${req.body.extension}` } } else if(i[i.length - 1] === "/" && i !== "" && i[i.length - 1] === "\\"){ return i += `*${req.body.extension}` } }).filter(i => i)
      }else if(parseInt(m) === 1){
        C =  c.split(",").map(function(i){ if(i !== "" && typeof i === "string"){ if (i.trim().split("")[i.trim().length-1].includes("/") === false){ if( ! i.includes(":") ) { return i+"/" } else { if (i.trim().split("")[i.trim().length-1].includes("\\")) { return i } else { return i+`\\` } } } else { return i } } else{ return i } }).filter(i => i)
      }
      return C.join(",");
    }
  }else if(t === "AG3"){
    let columns = eval(Object.entries(e).map(function(x){ if (x[0].includes(`${c}_selectCol`)) { return x.pop() } })).filter(e => e).join(",");
    return c + ":" + columns;
  }
}
// Format size.
function formatBytes(bytes, decimals = 0) {
    if (!+bytes) return '0 Bytes'

    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ['Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']

    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}

//Calculate rows
const calculateRows = (r, tr) => {
  let a = [];
  for (let i = 0; i <= tr; i++){
    a.push(i)
  }
  return a.slice(-r);
}

// Index Manage
controller.manager = (req, res) => {
  if (typeof req.session.userid === "undefined") {
    res.redirect(`/${process.env.SUBFOLDER}`)
  } else {
    const user = req.session.userid
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (err, words) => {
          let word = []
          let word1 = []
          for (let i in words) {
            word.push(words[i].words_id)
            word1.push(words[i].words_often)
          }
          if (err) { res.json(err) } else {
            res.render(`./agent/index_manage`, {
              history: history,
              words: words,
              words1: word,
              words2: word1,
              session: req.session
            })
          }
          funHistory.funchistory(req, "Agent Manage", "เข้าสู่เมนู สร้างการใช้งาน Agent", user)
        })
      })
    })
  }
}
// New manage
controller.newManager = (req,res) =>{
  if(typeof req.session.userid === 'undefined'){
    res.redirect(`/${process.env.SUBFOLDER}`)
  }else{
    const user = req.session.userid; 
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE status = 1 AND hide = 0 AND _limit_ != 0;', (err,agent) => {
            let word = []
            let word1 = []
            for (let i in words) {
              word.push(words[i].words_id)
              word1.push(words[i].words_often)
            }
            if(err){ res.json(err); }else{
              res.render(`./agent/newAgentToUse`,{
                agent: agent,
                history: history,
                words: words,
                words1: word,
                words2: word1,
                session: req.session
              })
            }
          })
        })
      })
    })
  }
}
// Add manage
controller.addManage = (req, res) => {
  if (typeof req.session.userid === 'undefined') {
    res.redirect(`/${process.env.SUBFOLDER}`)
  } else {
    req.getConnection((_, conn)=>{
      conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE ags_id = ?;',[req.body.ags_id],(_, store)=>{
        let count = parseInt(store[0]._limit_)-1;
        if(store[0].code === "AG1" ){
          let hashType = -2;
          if(typeof req.body.hash_type !== "undefined"){
            hashType = req.body.hash_type;
          }
          let body = {
            "agm_name": req.body.agm_name.trim(),
            "ags_id": req.body.ags_id,
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": modifyConfig(store[0].code, req.body.total_path, null, null),
            "agm_token": Base64.stringify(Utf8.parse(`${store[0].code}&&&${0}&&&${req.body.agm_name}&&&${HOST}&&&${PORT}&&&${modifyConfig(store[0].code, req.body.total_path, null, null)}`)),
            "hash_type": hashType
          }
          conn.query('INSERT INTO TB_TR_PDPA_AGENT_MANAGE SET ?;',[body],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }else if(store[0].code === "AG2"){
          let config = modifyConfig(store[0].code, req.body.total_path, req.body.extension, req.body.type_of_path)
          const body = {
            "agm_name": req.body.agm_name.trim(),
            "ags_id": req.body.ags_id,
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": config,
            "agm_token": Base64.stringify(Utf8.parse(`${store[0].code}&&&${0}&&&${req.body.agm_name}&&&${HOST}&&&${PORT}&&&${config}`))
          }
          conn.query('INSERT INTO TB_TR_PDPA_AGENT_MANAGE SET ?;',[body],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }else if(store[0].code === "AG3"){
          let config = `${req.body.service_database}&${req.body.host}&${req.body.username}&${req.body.password}&${req.body.database}&${modifyConfig(store[0].code, req.body.selectTable, req.body, null)}`
          const body = {
            "agm_name": req.body.agm_name.trim(),
            "ags_id": req.body.ags_id,
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": config,
            "agm_token": Base64.stringify(Utf8.parse(`${store[0].code}&&&${0}&&&${req.body.agm_name}&&&${HOST}&&&${PORT}&&&${config}`)),
          }
          conn.query('INSERT INTO TB_TR_PDPA_AGENT_MANAGE SET ?;',[body],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }else if(store[0].code === "AG4"){
          let config = `${req.body.mode},${req.body.path}`
          const body = {
            "agm_name": req.body.agm_name.trim(),
            "ags_id": req.body.ags_id,
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": config,
            "agm_token": Base64.stringify(Utf8.parse(`${store[0].code}&&&${0}&&&${req.body.agm_name}&&&${HOST}&&&${PORT}&&&${config}`))
          }
          conn.query('INSERT INTO TB_TR_PDPA_AGENT_MANAGE SET ?;',[body],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }
        conn.query(`UPDATE TB_TR_PDPA_AGENT_STORE SET _limit_ = ${count} WHERE ags_id = ${req.body.ags_id};`,(err, _)=>{
          if(err){
            res.json(err)
          } 
        })
        funHistory.funchistory(req, "Agent Manage", `เพิ่มข้อมูล สร้าง Service Agent ${req.body.agm_name}`, req.session.userid)
      })
    })
  }
}
// Detail manage
controller.detailManage = (req, res) =>{
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const user = req.session.userid;
    const {id} = req.params;
    req.getConnection((_, conn)=>{
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT * FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id WHERE agm_id = ?;',[id],(err,manage)=>{
            let word = []
            let word1 = []
            for (let i in words) {
              word.push(words[i].words_id)
              word1.push(words[i].words_often)
            }
            if (err) { res.json(err) } else {
              res.render(`./agent/detailAgentToUse`, {
                manage: manage,
                history: history,
                words: words,
                words1: word,
                words2: word1,
                session: req.session
              })
            }
            funHistory.funchistory(req, "Agent Manage", `ดูข้อมูล Service Agent ${manage[0].agm_name}`, user)
          })
        })
      })
    })
  }
}
// Step install client
controller.procedureManage = (req, res) =>{ if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }
  const user = req.session.userid;
  const {id} = req.params;
  req.getConnection((_, conn) =>{
    conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
      conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
        conn.query("SELECT * FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id WHERE agm_id = ?;", [id], (err,manage)=>{
          if(err) {res.json(err)};
          let word = []
          let word1 = []
          for (let i in words) {
            word.push(words[i].words_id)
            word1.push(words[i].words_often)
          }
          funHistory.funchistory(req, "Agent Manage", `ดูวิธีการติดตั้ง Service Agent ${manage[0].agm_name}`, user)
          res.render(`./agent/procedure`,{ manage, history:history, words1: word, words2: word1, session: req.session });
        })
      })
    })
  })
}
// Step install client (Windows)
controller.procedureManage1 = (req, res) =>{ if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }
  const user = req.session.userid;
  const {id} = req.params;
  req.getConnection((_, conn) =>{
    conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
      conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
        conn.query("SELECT * FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id WHERE agm_id = ?;", [id], (err,manage)=>{
          if(err) {
            res.json(err);
          }
          let word = []
          let word1 = []
          for (let i in words) {
            word.push(words[i].words_id)
            word1.push(words[i].words_often)
          }
          funHistory.funchistory(req, "Agent Manage", `ดูวิธีการติดตั้ง Service Agent ${manage[0].agm_name} บน Windows`, user)
          res.render(`./agent/procedure1`,{ manage, history:history, words1: word, words2: word1, session: req.session });
        })
      })
    })
  })
}
// Edit manage 
controller.editManager = (req, res) =>{
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const user = req.session.userid
    const {id} = req.params;
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE;',(_, agent)=>{
            conn.query('SELECT * FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id WHERE agm_id = ?;',[id],(err, manage)=>{
              let word = []
              let word1 = []
              for (let i in words) {
                word.push(words[i].words_id)
                word1.push(words[i].words_often)
              }
              if (err) { res.json(err) } else {
                res.render(`./agent/editAgentToUse`, {
                  agent: agent,
                  manage: manage,
                  history: history,
                  words: words,
                  words1: word,
                  words2: word1,
                  session: req.session
                })
              }
            })
          })
        })
      })
    }) 
  }
}
// Update manage
controller.updateManage = (req, res) =>{ 
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const {id} = req.params;
    req.getConnection((_ ,conn)=>{
      conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE ags_id = ?;',[req.body.ags_id],(_, store)=>{
        if(store[0].code === "AG1" ){ // Bug paths
          const body = {
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": modifyConfig(store[0].code, req.body.total_path, null, null)
          }
          conn.query('UPDATE TB_TR_PDPA_AGENT_MANAGE SET ? WHERE agm_id = ?;',[body, id],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }else if(store[0].code === "AG2"){ // Bug paths
          let config = modifyConfig(store[0].code, req.body.total_path, req.body.extension, req.body.type_of_path)
          const body = {
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": config
          }
          conn.query('UPDATE TB_TR_PDPA_AGENT_MANAGE SET ? WHERE agm_id = ?;',[body, id],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }else if(store[0].code === "AG3"){
          let config = `${req.body.service_database}&${req.body.host}&${req.body.username}&${req.body.password}&${req.body.database}&${modifyConfig(store[0].code, req.body.selectTable, req.body, null)}`
          const body = {
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": config
          }
          conn.query('UPDATE TB_TR_PDPA_AGENT_MANAGE SET ? WHERE agm_id = ?;',[body, id],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }else if(store[0].code === "AG4"){
          let config = `${req.body.mode},${req.body.path}`
          const body = {
            "acc_id": req.body.acc_id,
            "device_plugin": req.body.device_plugin.trim(),
            "ip_plugin": req.body.ip_plugin.trim(),
            "config_detail": config
          }
          conn.query('UPDATE TB_TR_PDPA_AGENT_MANAGE SET ? WHERE agm_id = ?;',[body, id],(err, _)=>{
            if(err){ res.json(err) }else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`)
            }
          })
        }
        funHistory.funchistory(req, "Agent Manage", `แก้ไขข้อมูล Service Agent ${req.body.agm_name}`, req.session.userid)
      })
    })
  }
}
// Delete manage
controller.deleteManage = (req, res) =>{
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const { id, index } = req.params;
    req.getConnection((_ ,conn)=>{
      conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE ags_id = ?;',[index],(_, store)=>{
        conn.query('SELECT * FROM TB_TR_PDPA_AGENT_MANAGE WHERE agm_id = ?;',[id],(_, manage)=>{
          conn.query('DELETE FROM TB_TR_PDPA_AGENT_LISTEN_HISTORY WHERE agm_id = ?;',[id],(err, _)=>{
            if(err){res.json(err)} 
          })
          conn.query('UPDATE TB_TR_PDPA_AGENT_STORE SET _limit_ = ? WHERE ags_id = ?;',[parseInt(store[0]._limit_)+1, index],(err, _)=>{
            if(err) {res.json(err)} 
          })
          conn.query('DELETE FROM TB_TR_PDPA_AGENT_MANAGE WHERE agm_id = ?;',[id],(err, _)=>{
            if(err){res.json(err)}else{
              res.redirect(`/${process.env.SUBFOLDER}agent_manage`);
            }
          })
          funHistory.funchistory(req, "Agent Manage", `ลบข้อมูล Service Agent ${manage[0].agm_name}`, req.session.userid)
        })
      })
    })
  }
}
// Send AJAX (POST)
controller.tablesGlobal = (req, res) => { // All display table agent.
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) } else {
    const hash = Base64.stringify(sha256(req.body.value));
    if(hash === "yG8z7OxbdbAYvKSFfu7nH23i91vPzeSCcGSuN5J5MFQ="){
      const {state} = req.body;
      // Main key.
      const limit = calculateRows(parseInt(state.rows), (parseInt(state.rows)*parseInt(state.page)));
      const start = (limit.length !== 0) ? limit.shift():state.page;
      const end = (limit.length !== 0) ? limit.pop():state.rows;
      // Option from agent file
      const totalLen = (e) =>{
        let a = 0
        e.forEach(function(r){
          a+=r.size
        })
        return a
      }
      // Option from agent database check
      const listColumns = (a, i, b) => {
        if (i === a.length){
          return b
        }else if(i === 9){
            b.push(`field_${0}`)
            return listColumns(a, (i+1), b)
        }else{
          b.push(`field_${i+1}`)
          return listColumns(a, (i+1), b)
        }
      }
      const columns = (s) => {
        if (Object.keys(s).find(e => e === 'from')){
          return listColumns(state['from'].split(":")[1].split(","), 0, []);
        }else{
          return []
        }
      }
      req.getConnection((_, conn) => {
        if (state.type === 'manage'){
          conn.query('SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_MANAGE;',(_, len) =>{
            if (len[0].count <= parseInt(state.rows)){
              conn.query('SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY agm_id DESC) as no, agm_id, agm_name, pas.ags_id, pas.code, pas.name, agm_created, CONCAT(firstname, " ", lastname) as fullname, (SELECT _get_ from TB_TR_PDPA_AGENT_LISTEN_HISTORY WHERE agm_id = pam.agm_id ORDER BY alh_id desc limit 0,1) as last_access, agm_status FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id JOIN TB_TR_ACCOUNT as acc ON pam.acc_id = acc.acc_id ORDER BY agm_id DESC) as DATA', (err, manage) => {
                if (err) { res.json(err) } else {
                  state.querySet = manage
                  res.json({state, len})
                }
              })
            }else{
              conn.query('SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY agm_id DESC) as no, agm_id, agm_name, pas.ags_id, pas.code, pas.name, agm_created, CONCAT(firstname, " ", lastname) as fullname, (SELECT _get_ from TB_TR_PDPA_AGENT_LISTEN_HISTORY WHERE agm_id = pam.agm_id ORDER BY alh_id desc limit 0,1) as last_access, agm_status FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id JOIN TB_TR_ACCOUNT as acc ON pam.acc_id = acc.acc_id ORDER BY agm_id DESC) as DATA WHERE no >= ? and no <= ?;', [start, end], (err, manage) => {
                if (err) { res.json(err) } else {
                  state.querySet = manage
                  res.json({state, len})
                }
              })
            }
          })
        }else if(state.type === 'store'){
          conn.query('SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_STORE;', (_, len)=>{
            if(len[0].count <= parseInt(state.rows)){
              conn.query('SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY ags_id) as no FROM TB_TR_PDPA_AGENT_STORE) as DATA;', (err, store)=>{
                if(err){ res.json(err) }else{
                  state.querySet = store;
                  res.json({state, len})
                }
              });
            }else{
              conn.query('SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY ags_id) as no FROM TB_TR_PDPA_AGENT_STORE) as DATA WHERE no >= ? AND no <= ?;', [start, end], (err, store)=>{
                if(err){ res.json(err) }else{
                  state.querySet = store;
                  res.json({state, len})              
                }
              });
            }
          });
        }else if(state.type === 'file'){
          conn.query('SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_FILE_DIR;', (_, len) => {
            conn.query('SELECT device_name FROM TB_TR_PDPA_AGENT_FILE_DIR GROUP BY device_name;', (_, allDevice) => {
              if(len[0].count <= parseInt(state.rows)){
                conn.query('SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY id DESC) as no FROM TB_TR_PDPA_AGENT_FILE_DIR ORDER BY id DESC) as DATA;', (err, dirDevice) => {
                  if (err) {res.json(err)}else{
                    state.querySet = dirDevice;
                    res.json({ dir: allDevice.length, len, state, total: totalLen(dirDevice) });
                  }
                })
              }else{
                conn.query('SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY id DESC) as no FROM TB_TR_PDPA_AGENT_FILE_DIR ORDER BY id DESC) as DATA WHERE no >= ? AND no <= ?;', [start, end], (err, dirDevice) => {
                  if (err){ res.json(err) }else{
                    state.querySet = dirDevice;
                    res.json({ dir: allDevice.length, len, state, total: totalLen(dirDevice) });
                  }
                })
              }
            })
          })
        }else if(state.type === 'database'){
          const from = (s) => { if(Object.keys(s).find(e => e === 'from')){ return s.from }else{ return null }};
          conn.query(`SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = ?;`, [from(state)], (_, len) =>{
            
            if(len[0].count <= parseInt(state.rows)){
              conn.query(`SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY id) as no, ${columns(state)}, _get, id, from_client FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = "${from(state)}") as DATA;`, (err, selected) =>{
                if(err) { res.json(err) }else{
                  state.querySet = selected
                  res.json({state, len})
                }
              })
            }else{
              conn.query(`SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY id) as no, ${columns(state)}, _get, id, from_client FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = "${from(state)}") as DATA WHERE no >= ? AND no <= ?;`, [start, end], (err, selected) =>{
                if(err) { res.json(err) }else{
                  state.querySet = selected
                  res.json({state, len})
                }
              })
            }
          })
        }else if(state.type === 'log0'){
          conn.query('SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_LOG0_HASH;', (_, len) => {
            if(len[0].count <= parseInt(state.rows)){
              conn.query('SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY id DESC) as no, id, device_name,os_name,path,name_file,total_line,DATE_FORMAT(date_now, "%d/%m/%Y %H:%i") as date_now, value FROM TB_TR_PDPA_AGENT_LOG0_HASH ORDER BY id DESC) as DATA;', (err, hash) => {
                if (err) { res.json(err) } else {
                  state.querySet = hash;
                  res.json({ state, len });
                }
              })
            }else{
              conn.query('SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY id DESC) as no, id, device_name,os_name,path,name_file,total_line,DATE_FORMAT(date_now, "%d/%m/%Y %H:%i") as date_now, value FROM TB_TR_PDPA_AGENT_LOG0_HASH ORDER BY id DESC) as DATA WHERE no >= ? AND no <= ? ORDER BY id DESC;', [start, end], (err, hash) => {
                if (err) { res.json(err) } else {
                  state.querySet = hash;
                  res.json({ state, len });
                }
              })
            }
          })
        }else if(state.type === 'sniffer'){
          // Option from agent sniffer
          let files = fs.readdirSync(state.from).filter(i => i !== ".DS_Store" && (i.includes('.log') || i.includes('.snf')));
          let result = [];
          if (files.length <= parseInt(state.rows) && parseInt(state.page) === 1){
            for (let i in files){
              result.push({ "no": parseInt(i)+1, "name": files[i].split(".").splice('snf', (files[i].split(".").length - 1)).join("."), "size": fs.statSync(state.from+files[i]).size, "extension": path.extname(files[i]) })
            }
          }else{
            limit.unshift(start), limit.push(end)
            for(let i = start - 1; i < end; i++){
              let convi = ""
              if(i.toString().length === 1 && i === 0){
                convi = (i !== 0) ? Number((i/10).toString().split('.')[1]) : 0
              }else{
                convi = (Number(i.toString().split("")[i.toString().split("").length - 1]) !== 0) ? Number((i/10).toString().split('.')[(i/10).toString().split('.').length - 1]) : 0
              }
              if(typeof files[i] !== 'undefined'){
                result.push({ "no": limit[convi], "name": files[i].split(".").splice('snf', (files[i].split(".").length - 1)).join("."), "size": fs.statSync(state.from+files[i]).size, "extension": path.extname(files[i]) })
              }
            }
          }
          state.querySet = result;
          res.json({state, len: files.length})
        }else{
          res.json({message: "404"})
        }
      });
    }else{
      console.log(req.body);
    }
  }
}
controller.chartsGlobal = (req, res) => { // Charts all agent.
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const hash = Base64.stringify(sha256(req.body.value))
    if(hash === "4mi/BXECABqoWklna27mDsxu0arT/H5PE3oSI5jeZO4="){
      req.getConnection((_, conn) =>{
        if(req.body.type === 'file'){
          conn.query('SELECT _get as date, COUNT(*) as count FROM TB_TR_PDPA_AGENT_FILE_DIR GROUP BY _get;', (err, result) => {
            if(err) {res.json(err)}
            res.json(result);
          })
        }else if(req.body.type === 'database'){
          conn.query('SELECT _get as date, COUNT(*) as count FROM TB_TR_PDPA_AGENT_DATABASE_CHECK GROUP BY _get;', (err, result) =>{
            if(err) { res.json(err) }else{
              res.json(result)
            }
          })
        }else if(req.body.type === 'log0'){
          conn.query('SELECT date_now as date, COUNT(*) as count FROM TB_TR_PDPA_AGENT_LOG0_HASH GROUP BY date_now;', (err, result) => {
            if (err) { res.json(err) } else {
              res.json(result);
            }
          })
        }else if(req.body.type === 'sniffer'){
          const dir = path.join(process.cwd(), './path/agent_sniffer/')
          let _path_ = fs.readdirSync(dir, { withFileTypes: true }).filter(dir => dir.isDirectory()).map(dir => dir.name);
          let result = []
          _path_.forEach(i => {
            let list = fs.readdirSync(dir+i).filter(i => i.includes('.log') || i.includes('.snf'));
            result.push({'data': _path_, 'len': list.length})
          })
          res.json(result)
        }
      })
    }else{
      console.log(req.body)
    }
  }
}
controller.nameManage = (req, res) => { // Query all name agent.
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const hash = Base64.stringify(sha256(req.body.value));
    if(hash === "rTeGf4BCgXVGQ5VrGVPIFLZx0J7vsuXfpqW6ZY/9s0Q="){
      req.getConnection((_, conn) => {
        conn.query('SELECT agm_name FROM TB_TR_PDPA_AGENT_MANAGE;', (err, result) => {
          if(err){ res.json(err) }
          res.json(result);
        })
      })
    }else{
      console.log(req.body)
    }
  }
}
controller.selectManage = (req, res) =>{ // Data to show modal delete manage
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const {id, value} = req.body;
    const hash = Base64.stringify(sha256(value))
    if(hash === "L2CU1QLqrnLS5/oZPo2E9UitCecrxBo+AHmidGwA9W4="){
      req.getConnection((_, conn)=>{
        conn.query('SELECT * FROM TB_TR_ACCOUNT;',(_, account)=>{
          conn.query('SELECT * FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id WHERE agm_id = ?;',[id],(err, manage)=>{
            if(err){res.json(err)}else{
              res.json({manage, account})
            }
          })
        })
      })
    }else{
      console.log(req.body)
    }
  }
}
controller.updateStatus = (req,res) =>{ // Update status agent manage with selected
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const { value, status } = req.body;
    const hash = Base64.stringify(sha256(value));
    if(hash === "bDQmT7ynPWpQt3ZbV6/RiV1UrCPbd3+WZE8Z09TGOAU="){
      const {id} = req.params;
      req.getConnection((_, conn)=>{
        conn.query('SELECT * FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id WHERE agm_id = ?;',[id],(err, manage)=>{
          if(err){ 
            res.json(err) 
          }else if(manage.length > 0){
            if(parseInt(manage[0].agm_status) == 0){
              conn.query('UPDATE TB_TR_PDPA_AGENT_MANAGE SET agm_status = ? WHERE agm_id = ?;',[status, id],(err,pass)=>{
                if(err){ res.json(err) }else{
                  res.json(pass)
                }
              })
            }else if(parseInt(manage[0].agm_status) == 1){
              conn.query('UPDATE TB_TR_PDPA_AGENT_MANAGE SET agm_status = ? WHERE agm_id = ?;',[status, id],(err,pass)=>{
                if(err){ res.json(err) }else{
                  res.json(pass)
                }
              })
            }
          }
        })
      })
    }else{
      console.log(req.body)
    }
  }
}
controller.selectStore = (req, res) =>{ // Table agent store page
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const {id, value} = req.body;
    const hash = Base64.stringify(sha256(value))
    if (hash === "fkrG+10zwl5CXz38BtA+keOm6e2FrI6hDLWX3VDOX/8="){
      req.getConnection((_, conn)=>{
        conn.query("SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE ags_id = ?;",[id],(err, store)=>{
          if(err){res.json(err)}else{
            res.json(store)
          }
        })
      })
    }else{
      console.log(req.boy)
    }
  }
}
controller.fileLogAg1Detail = (req, res) => { // Modal detail agent file/dir with selected
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    const { id } = req.body;
    req.getConnection((_, conn) => {
      conn.query('SELECT * FROM TB_TR_PDPA_AGENT_FILE_DIR WHERE id = ?;', [id], (err, dirDevice) => {
        if (err) {res.json(err)}
        res.json(dirDevice);
      })
    })
  }
}
controller.testConnect = (req,res) =>{ // API send agent listen to get quert content from database with select
  if(typeof req.session.userid === "undefined") { res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const getValue = req.body.value;
    const hash = Base64.stringify(sha256(getValue));
    if(hash === "kwUwAEnFntblMRkVv41u3/tWDZFHmD9jLsgNDSHWJ8U="){
      const { host, user, pass, db, type } = req.body;
      if (parseInt(type) === 1 || parseInt(type) === 0){
        let message = "";
        if (parseInt(type) === 1){
          message = "MySQL สามารถเชื่อมต่อได้."
        }else if(parseInt(type) === 0){
          message = "Oracle DB สามารถเชื่อต่อได้."
        }
        let tables = "";
        new Promise((resolve, _) => {
          const client = net.createConnection({port: PORT, host: HOST}, () => {
            resolve(client);
          });
          client.on('error', err => res.json({err}));
          client.write(`${type}|${host}|${user}|${pass}|${db}`);
          client.end();
        }).then(connection => {
            connection.on('data', data =>{
              let err = {};
              try {
                tables = JSON.parse(data.toString());
              } catch (e) {
                if (e.toString().includes(`SyntaxError: Unexpected token 'e', "error retu"... is not valid JSON`) || e.toString().includes(`OCI Error:`) || e.toString().includes("DPI Error")) {
                  err = {
                    code: data.toString().split(":")[1],
                    err: data.toString().split(":")[2]
                  }
                } else {
                  tables += data.toString()
                }
              } finally {
                if (Object.keys(err).length > 0) {
                  res.json(err);
                } else if (typeof tables == "string" && tables[tables.length - 1].includes("]")) {
                  res.json({
                    result: (typeof tables == "object") ? tables: JSON.parse(tables), 
                    message
                  });
                } else {
                  res.json({
                    result: tables, 
                    message
                  });
                }
              } 
            });
          })
      }else{
        let message = "ระบบไม่รองรับ. กรุณาเลือกตามที่แสดงผล!!"
        res.json({message})
      }
    }else{
      console.log(req.body)
    }
  }
}
controller.databaseAg3 = (req, res) => { // Select to show detail to delete row
  if (typeof req.session.userid === 'undefined') { 
    res.redirect(`/${process.env.SUBFOLDER}`); 
  } else if (req.body.id){
    const {id} = req.body;
    req.getConnection((_, conn) => {
      conn.query('SELECT * FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE id = ?',[id],(_, database) => {
        const columns = database[0].from_client.split(":")[1].split(",")
        const listColumns = (c, i, a) => {
          if(i === c.length){
            return a
          } 
          a.push(`field_${i+1}`)
          return listColumns(c, (i+1), a)
        }
        conn.query(`SELECT id,${listColumns(columns, 0, [])},_get FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE id = ?;`, [id], (err, selected) =>{
          if (err) {
            res.json(err)
          } else {
            res.json({
              data: selected,
              column: columns,
            });
          }
        })
      })
    });
  }
}
controller.loggerAg2 = (req, res) => { // Modal select display agent log0 hash
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    const {id} = req.body;
    req.getConnection((_, conn) => {
      conn.query('SELECT id, device_name,os_name,path,name_file,total_line,DATE_FORMAT(date_now, "%d/%m/%Y %H:%i") as date_now, value FROM TB_TR_PDPA_AGENT_LOG0_HASH WHERE id = ?',[id], (_, hash) => {
        conn.query('SELECT * FROM TB_MM_SET_SYSTEM ORDER BY sys_id DESC LIMIT 1;', (err, sys) =>{
          if (err) {
            res.json(err)
          } else {
            res.json({ hash, sys });
          }
        })
      })
    })
  }
}
controller.selectSniffer = (req, res) =>{ // Modal show content file agent sniffer page
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const hash = Base64.stringify(sha256(req.body.value))
    if(hash === "H5B34RecpaLM0NPclLyC5Z3+3fImERiuFnWUw5FWsGg="){
      // Option from agent sniffer
      let files = fs.readFileSync(req.body.from, 'utf8').toString();
      res.json(files)
    }else{
      console.log(req.body)
    }
   }
 }
controller.searchGlobal = (req, res) =>{ // Search data all agent
  if(typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`); }else{
    if(Base64.stringify(sha256(req.body.value)) !== "sFN1sLGU2NgyHaVELvJdjcEL3zBByFVV/eFfrXXzS54=") {
      res.json(req.body)
    }
    const { from, data, more } = req.body;
    req.getConnection((_, conn) => {
      if (from === "log0"){
        conn.query(`SELECT * FROM TB_TR_PDPA_AGENT_LOG0_HASH WHERE device_name LIKE "%${data}%" OR os_name LIKE "%${data}%" OR path LIKE "%${data}%" OR name_file LIKE "%${data}%";`,(err, result) =>{
          if(err) {res.json(err)}
          res.json(result);
        })
      }else if(from === "file"){
        conn.query(`SELECT * FROM TB_TR_PDPA_AGENT_FILE_DIR WHERE name_file LIKE "%${data}%";`,(err, result) =>{
          if(err) {res.json(err)}
          res.json(result);
        })
      }else if(from === "database"){
        let field = [];
        let likeField = [];
        for (let i = 0; i < more.split(":")[more.split(":").length - 1].split(",").length; i++){
          if(parseInt(i)+1 === more.split(":")[more.split(":").length - 1].split(",").length){
            if(parseInt(i)+1 === 10){
              field.push(`field_0`);
              likeField.push(`client.field_0 LIKE "%${data}%";`);
            }else{
              field.push(`field_${i+1}`)
              likeField.push(`client.field_${i+1} LIKE "%${data}%";`);
            }
          }else{
            field.push(`field_${i+1}`)
            likeField.push(`client.field_${i+1} LIKE "%${data}%" OR`);
          }
        }
        conn.query(`SELECT * FROM (SELECT ${String(field)} FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = "${more}") as client WHERE ${likeField.join(" ")}`, (err, result) =>{
          if(err) {res.json(err)}
          res.json(result);
        })
      }else if(from === "sniffer"){
        let files = fs.readdirSync(more, { withFileTypes: true }).filter(dir => dir.isFile()).map(dir => dir.name).filter(i => i !== ".DS_Store" && (i.includes('.snf') || i.includes('.log')));
        res.json(files)
      }else if(from === "manage"){
        conn.query(`SELECT * FROM (SELECT pam.agm_name, pas.code, pas.name, CONCAT(a.firstname, ' ' ,a.lastname) as fullname FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id JOIN TB_TR_ACCOUNT as a ON pam.acc_id = a.acc_id) as manage WHERE manage.agm_name LIKE "%${data}%" OR manage.code LIKE "%${data}%" OR manage.name LIKE "%${data}%" OR manage.fullname LIKE "%${data}%";`, (err,result) =>{
          if(err) {res.json(err)};
          res.json(result);
        })
      }else{
        res.json({message: "404"})
      }
    });
  }
}
controller.selectSearchGlobal = (req, res) =>{ // After seacrch and responese data to show table agent.
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`); }else{
    if(Base64.stringify(sha256(req.body.value)) !== "3W1h5UZkOe4FRuaX8Cm6ZV0/dULCRPewMK1R3XZJ7Do=") {
      res.json(req.body);
    } 
    // Main varriable
    const {state} = req.body;
    const limit = calculateRows(parseInt(state.rows), (parseInt(state.rows)*parseInt(state.page)));
    const start = (limit.length !== 0) ? limit.shift():state.page;
    const end = (limit.length !== 0) ? limit.pop():state.rows;
    const selectData = state.select;
    // Option
    let selectKey = (typeof state.key === 'undefined') ? state.from:state.key;
    // Option from agent file
    const totalLen = (e) =>{
      let a = 0
      e.forEach(function(r){
        a+=r.size
      })
      return a
    }
    req.getConnection((_, conn) => {
      if( state.type === "log0" ){
        conn.query(`SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_LOG0_HASH WHERE device_name LIKE "%${selectData}%" OR os_name LIKE "%${selectData}%" OR path LIKE "%${selectData}%" OR name_file LIKE "%${selectData}%";`,(_, len)=>{
          if(len[0].count <= parseInt(state.rows)){
            conn.query(`SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY id) as no FROM TB_TR_PDPA_AGENT_LOG0_HASH WHERE device_name LIKE "%${selectData}%" OR os_name LIKE "%${selectData}%" OR path LIKE "%${selectData}%" OR name_file LIKE "%${selectData}%") as DATA;`,(err, result) =>{
              if(err){ res.json(err) }else{
                state.querySet = result;
                res.json({state, len})
              }
            })
          }else{
            conn.query(`SELECT * FROM SELECT *, ROW_NUMBER() OVER(ORDER BY id) as no FROM TB_TR_PDPA_AGENT_LOG0_HASH WHERE device_name LIKE "%${selectData}%" OR os_name LIKE "%${selectData}%" OR path LIKE "%${selectData}%" OR name_file LIKE "%${selectData}%") as DATA WHERE no >= ${start} AND no <= ${end};`,(err, result) =>{
              if(err){ res.json(err) }else{
                state.querySet = result;
                res.json({state, len})
              }
            })
          }
        })
      }else if( state.type === "file" ){
        conn.query(`SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_FILE_DIR WHERE name_file LIKE "%${selectData}%";`,(_, len)=>{
            conn.query('SELECT device_name FROM TB_TR_PDPA_AGENT_FILE_DIR GROUP BY device_name;', (_, allDevice) => {
            if(len[0].count <= parseInt(state.rows)){
              conn.query(`SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY id) as no FROM TB_TR_PDPA_AGENT_FILE_DIR WHERE name_file LIKE "%${selectData}%") as DATA;`, (err, result) => {
                if(err){ res.json(err) }else{
                  state.querySet = result;
                  state.option = totalLen(allDevice);
                  res.json({state, len})
                }
              })
            }else{
              conn.query(`SELECT * FROM (SELECT *, ROW_NUMBER() OVER(ORDER BY id) as no FROM TB_TR_PDPA_AGENT_FILE_DIR WHERE name_file LIKE "%${selectData}%") as DATA WHERE no >= ${start} AND no <= ${end};`, (err, result) => {
                if(err){ res.json(err) }else{
                  state.querySet = result;
                  state.option = totalLen(allDevice);
                  res.json({state, len})
                }
              })
            }
          })
        });
      }else if( state.type === "database" ){
        let field = [];
        let likeField = [];
        for (let i = 0; i < selectKey.split(":")[selectKey.split(":").length - 1].split(",").length; i++){
          if(parseInt(i)+1 === selectKey.split(":")[selectKey.split(":").length - 1].split(",").length){
            if(parseInt(i)+1 === 10){
              field.push(`field_0`);
              likeField.push(`field_0 LIKE "%${selectData}%"`);
            }else{
              field.push(`field_${i+1}`)
              likeField.push(`field_${i+1} LIKE "%${selectData}%"`);
            }
          }else{
            field.push(`field_${i+1}`)
            likeField.push(`field_${i+1} LIKE "%${selectData}%" OR`);
          }
        }
        conn.query(`SELECT COUNT(*) as count FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = "${selectKey}" AND ${likeField.join(" ")};`,(_, len) =>{
          if(len[0].count <= parseInt(state.rows)){
            conn.query(`SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY id) as no, ${field.join(",")}, _get, id FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = "${selectKey}" AND ${likeField.join(" ")}) as DATA;`,(err, result) =>{
              if(err) { res.json(err) }else{
                state.querySet = result;
                res.json({state, len})
              }
            })
          }else{
            conn.query(`SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY id) as no, ${field.join(",")}, _get, id FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE from_client = "${selectKey}" AND ${likeField.join(" ")}) as DATA WHERE no >= ${start} AND no <= ${end};`,(err, result) =>{
              if(err) { res.json(err) }else{
                state.querySet = result;
                res.json({state, len})
              }
            })
          }
        });
      }else if( state.type === "sniffer" ){
        let files = fs.readdirSync(selectKey, { withFileTypes: true }).filter(dir => dir.isFile()).map(dir => dir.name).filter(i => i !== ".DS_Store" && (i.includes('.snf') || i.includes('.log')));
        if (selectKey.split("/")[selectKey.split("/").length - 1] !== "") {
          selectKey += "/";
        }
        // Get only selected.
        let total = files.map(i => (i.includes(selectData)) ? i:"").filter(i => i !== "");
        let result = []
        if(total.length <= parseInt(state.rows) && parseInt(state.page) === 1){
          total.forEach(function(i, n){
            if(typeof i !== 'undefined'){
              let status = fs.statSync(`${selectKey}${i}`)
              let name = i.split('.')
              name = name.splice('snf', (name.length-1)).join(".")
              result.push({"no": parseInt(n)+1, "name": name, "size": status.size, "extension": path.extname(i)})
            }
          })
        }else{
          limit.unshift(start), limit.push(end)
          for(let i = start - 1; i < end; i++){
            let convi = ""
            if(i.toString().length === 1 && i === 0){
              convi = (i !== 0) ? Number((i/10).toString().split('.')[1]) : 0
            }else{
              convi = (Number(i.toString().split("")[1]) !== 0) ? Number((i/10).toString().split('.')[1]) : 0
            }
            if(typeof total[i] !== 'undefined'){
              result.push({ "no": limit[convi], "name": files[i].split(".").splice('snf', (files[i].split(".").length - 1)).join("."), "size": fs.statSync(state.from+files[i]).size, "extension": path.extname(files[i]) })
            }
          }
        }
        state.querySet = result
        res.json({state, len: total.length})
      }else if( state.type === "manage" ){
        conn.query(`SELECT COUNT(*) as count FROM (SELECT pam.agm_name, pas.code, pas.name, CONCAT(acc.firstname, ' ', acc.lastname) as fullname FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id JOIN TB_TR_ACCOUNT as acc ON pam.acc_id = acc.acc_id) as manage WHERE agm_name LIKE "%${selectData}%" OR code LIKE "%${selectData}%" OR name LIKE "%${selectData}%" OR fullname LIKE "%${selectData}%";`, (_, len) => {
          if(len[0].count <= parseInt(state.rows)){
            conn.query(`SELECT * FROM (SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY pam.agm_id) as no, pam.agm_id, pam.agm_name, pas.code, pas.name, CONCAT(acc.firstname, ' ', acc.lastname) as fullname, pam.agm_created, pam.agm_status, (SELECT _get_ from TB_TR_PDPA_AGENT_LISTEN_HISTORY WHERE agm_id = pam.agm_id ORDER BY alh_id desc limit 0,1) as last_access FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id JOIN TB_TR_ACCOUNT as acc ON pam.acc_id = acc.acc_id) as manage WHERE agm_name LIKE "%${selectData}%" OR code LIKE "%${selectData}%" OR name LIKE "%${selectData}%" OR fullname LIKE "%${selectData}%") as DATA;`, (err, result) => {
              if(err){ res.json(err) }else{
                state.querySet = result;
                res.json({state, len})
              }
            })
          }else{
            conn.query(`SELECT * FROM (SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY pam.agm_id) as no, pam.agm_id, pam.agm_name, pas.code, pas.name, CONCAT(acc.firstname, ' ', acc.lastname) as fullname, pam.agm_created, pam.agm_status, (SELECT _get_ from TB_TR_PDPA_AGENT_LISTEN_HISTORY WHERE agm_id = pam.agm_id ORDER BY alh_id desc limit 0,1) as last_access FROM TB_TR_PDPA_AGENT_MANAGE as pam JOIN TB_TR_PDPA_AGENT_STORE as pas ON pam.ags_id = pas.ags_id JOIN TB_TR_ACCOUNT as acc ON pam.acc_id = acc.acc_id) as manage WHERE agm_name LIKE "%${selectData}%" OR code LIKE "%${selectData}%" OR name LIKE "%${selectData}%" OR fullname LIKE "%${selectData}%") as DATA WHERE no >= ${start} AND no <= ${end};`, (err, result) => {
              if(err){ res.json(err) }else{
                state.querySet = result;
                res.json({state, len})
              }
            })
          }
        })
      }else{
        res.json({message: "404"})
      }
    });
  }
}
// File/Log 
controller.fileLogAg = (req, res) => {
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    const user = req.session.userid;
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT * FROM TB_TR_PDPA_AGENT_FILE_DIR;', (err, dirDevice) => {
            if(err) {res.json(err)}
            let word = []
            let word1 = []
            for (let i in words) {
              word.push(words[i].words_id)
              word1.push(words[i].words_often)
            }
            const totalFile = (t, e, i) =>{
              if(i === e.length){
                return t
              }else if(e[i].size !== null){
                t += parseInt(e[i].size)
                return totalFile(t, e, (i+1))
              }else{
                return totalFile(t, e, (i+1))
              }
            }
            res.render(`./agent/file_log_ag`, {
              dir: dirDevice.length,
              files: dirDevice,
              history: history,
              words: words,
              words1: word,
              words2: word1,
              total_file: formatBytes(totalFile(0, dirDevice, 0)),
              session: req.session
            });
            funHistory.funchistory(req, "Agent File/Log", `เข้าสู่เมนู ข้อมูล Agent File/Log`, user)
          })
        });
      });
    })
  }
}
// Database check
controller.databaseAg = (req, res) => {
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`);
  } else {
    const user = req.session.userid;
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT from_client FROM TB_TR_PDPA_AGENT_DATABASE_CHECK GROUP BY from_client;',(_, client) => {
            conn.query('SELECT * FROM TB_TR_PDPA_AGENT_DATABASE_CHECK;', (err, database) => {
              let word = []
              let word1 = []
              for (let i in words) {
                word.push(words[i].words_id)
                word1.push(words[i].words_often)
              }
              if (err) {
                res.json(err)
              }
              res.render(`./agent/database_ag`, {
                data0: client,
                data: database,
                history: history,
                words: words,
                words1: word,
                words2: word1,
                session: req.session
              });
              funHistory.funchistory(req, "Agent Database", `เข้าสู่เมนู ข้อมูล Agent Database`, user)
            })
          });
        });
      })
    })
  }
}
// Delete Record (Datacheck)
controller.delDatabaseAg = (req, res) => {
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    const data_id = req.params;
    const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
    req.getConnection((_, conn) => {
      conn.query("DELETE FROM TB_TR_PDPA_AGENT_DATABASE_CHECK WHERE id = ?", [data_id], (err, _) => {
        if (err) {
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        }
        res.redirect(req.get('referrer'));
      });
      res.redirect(req.get('referrer'));
      funHistory.funchistory(req, "Agent Database", `ลบข้อมูล Agent Database`, req.session.userid)
    });
  }
}
// Log0 hash
controller.loggerAg = (req, res) => {
  if (typeof req.session.userid === 'undefined') { res.redirect(`/${process.env.SUBFOLDER}`); } else {
    const user = req.session.userid;
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT id, device_name,os_name,path,name_file,total_line,DATE_FORMAT(date_now, "%d/%m/%Y %H:%i") as date_now, value FROM TB_TR_PDPA_AGENT_LOG0_HASH;', (_, hash) => {
            conn.query('SELECT path, count(path) as cpath FROM TB_TR_PDPA_AGENT_LOG0_HASH GROUP BY path;', (err, path) => {
              let word = []
              let word1 = []
              for (let i in words) {
                word.push(words[i].words_id)
                word1.push(words[i].words_often)
              }
              if (err) {res.json(err)}
              res.render(`./agent/logger_hash_ag`, {
                hash: hash,
                path: path,
                history: history,
                words: words,
                words1: word,
                words2: word1,
                session: req.session
              });
              funHistory.funchistory(req, "Agent Logger", `เข้าสู่เมนู ข้อมูล Agent Database`, user)
            })
          })
        });
      });
    })
  }
}
// Sniffer
controller.sniffer = (req, res) =>{
  if(typeof req.session.userid == 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const user = req.session.userid;
    const folder_sniffer = path.join(process.cwd(), './path/agent_sniffer/');
    req.getConnection((_ , conn) => {
      conn.query('SELECT DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => { conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (err, words) => {
        let word = []
        let word1 = []
        for (let i in words) {
          word.push(words[i].words_id)
          word1.push(words[i].words_often)
        }
        if (err) {
          res.json(err)
        }else{
          let obj = [];
          let path = fs.readdirSync(folder_sniffer, { withFileTypes: true }).filter(dir => dir.isDirectory()).map(dir => dir.name);
          path.forEach(i =>{
            if(i != '.DS_Store' && i.split("")[0] !== "."){
              obj.push({"name": i, "files": fs.readdirSync(folder_sniffer+i)})
            }
          })
          res.render(`./agent/sniffer`, {
            fullpath: folder_sniffer,
            total_path: obj,
            history: history,
            words: words,
            words1: word,
            words2: word1,
            session: req.session
          });
          funHistory.funchistory(req, "Agent Sniffer", `เข้าสู่เมนู ข้อมูล Agent Sniffer`, user)
        }
      });
      });
    })
  }
}
// Store
controller.store = (req, res) =>{
  if (typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const user = req.session.userid;
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (err, words) => {
          let word = []
          let word1 = []
          for (let i in words) {
            word.push(words[i].words_id)
            word1.push(words[i].words_often)
          }
          if (err) {
            res.json(err)
          }else{
            res.render(`./agent/store`, {
              history: history,
              words: words,
              words1: word,
              words2: word1,
              session: req.session
            });
            funHistory.funchistory(req, "Agent Store", `เข้าสู่เมนู ข้อมูล Agent Store`, user)
          }
        });
      });
    })
  }
}
// Update status
controller.updateStore = (req, res) =>{
  if(typeof req.session.userid === "undefined"){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const getValue = req.body.value;
    const hash = Base64.stringify(sha256(getValue));
    if(hash === "9E/clr1SIYX6yb2seanamVImfJP20wUyY2Mae99rkUQ="){
      const {id} = req.params;
      const {status} = req.body;
      req.getConnection((_,conn) =>{ 
        conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE ags_id = ?;',[id],(err,store)=>{
          if(err){ res.json(err) }
          else if(store.length > 0){
            conn.query('UPDATE TB_TR_PDPA_AGENT_STORE SET status = ? WHERE ags_id = ?;',[status,id],(err,pass)=>{
              if(err){ res.json(err) }else{
                res.json(pass)
              }
            })
          }
        })
      })
    }else{
      console.log(req.body)
    }
  }
}
// Detail service
controller.detailStore = (req, res) =>{
  if (typeof req.session.userid === 'undefined'){ res.redirect(`/${process.env.SUBFOLDER}`) }else{
    const user = req.session.userid;
    const {id} = req.params;
    req.getConnection((_, conn) => {
      conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from TB_TR_PDPA_DOCUMENT as d join TB_TR_PDPA_DOCUMENT_LOG as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC LIMIT 30;', [user], (_, history) => {
        conn.query('SELECT * FROM TB_MM_PDPA_WORDS', (_, words) => {
          conn.query('SELECT * FROM TB_TR_PDPA_AGENT_STORE WHERE ags_id = ?;',[id],(err, store) => {
            let word = []
            let word1 = []
            for (let i in words) {
              word.push(words[i].words_id)
              word1.push(words[i].words_often)
            }
            if (err) {
              res.json(err)
            }else{
              res.render(`./agent/detailStore`, {
                service: store,
                history: history,
                words: words,
                words1: word,
                words2: word1,
                session: req.session
              });
              funHistory.funchistory(req, "Agent Store", `ดูข้อมูล Agent Store`, user)
            }
          });
        });
      });
    })
  }
}

module.exports = controller
