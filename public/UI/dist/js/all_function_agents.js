const subfolder = document.getElementById('SubDomain').value;
const convert_datetime = (d, o) => {
  console.log(d, o);
  let time;
  let seconds;
  if (o.includes(" ")){
    time = o.split(" ")[1].split(":")
    seconds = time[time.length -1]
  }else{
    time = o.split("T")[1].split(":")
    seconds = time[time.length - 1].split(".")[0]
  }
  return `${("0"+d.getDate()).slice(-2)}/${("0"+(d.getMonth()+1)).slice(-2)}/${d.getFullYear()} ${time[0]}:${time[1]}:${seconds}`
}
//Convert date for sql to dd/mm/yyyy
const convert_date = (d) => {
  return `${("0"+d.getDate()).slice(-2)}/${("0"+(d.getMonth()+1)).slice(-2)}/${d.getFullYear()}`
}
const checkSameInner = (c, i) =>{
  if(i === c.length){
    return false
  }else if(c[i].innerHTML === c[(c.length - 1) - i].innerHTML){
    return true
  }else{
    return checkSameInner(c, (i+1))
  }
};
const containsOnlyNumbers = (s) =>{
  return /^[0-9]+$/.test(s);
}
//Convert size file
function convert_size(number) {
  let kb = number / 1000
  let mb = 0;
  let gb = 0; 
  if (kb > 1024) {
    mb = kb / 1000
  } else {
    return kb.toFixed(1) + " KB"
  }
  if (mb > 1024) {
    gb = mb / 1000
  } else {
    return mb.toFixed(1) + " MB"
  }
  if (gb < 1024) {
    return gb.toFixed(1) + "GB"
  }
}
// Function for new and edit agent manage sniffer.
function handclick(v){
    if(document.getElementById('mode').checked && v === "0"){
      $('div#subject-mode').html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> ระบุที่อยู่ของไฟล์เพื่อจัดเก็บ </th>
<td style="text-align: left;">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" /><br/>
<u>ตัวอย่าง</u>: ./sniffer/ หรือ /home/user/sniffer/
</td>
</table>
`);
    }else{ $('div#subject-mode').empty();
    }
  }
//Input seacrh
const searchData = (t, r, k) => {
  $.post(`/${subfolder}agent/search`,{ value: "@lltr@@gentSe@rchR@w", from: t, data: r, more: k}).done(function(res){
    if(r !== ""){
      // Option agent file and log0
      if(Object.keys(res).length !== 0) {
        $('datalist#browers').empty();
        if( t === "log0"){
          res.forEach(function(e){
            
            if(e.device_name.toLowerCase().includes(r.toLowerCase()) || e.device_name.toUpperCase().includes(r.toUpperCase()) || e.device_name.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.device_name));
            if(e.os_name.toLowerCase().includes(r.toLowerCase()) || e.os_name.toUpperCase().includes(r.toUpperCase()) || e.os_name.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.os_name));
            if(e.path.toLowerCase().includes(r.toLowerCase()) || e.path.toUpperCase().includes(r.toUpperCase()) || e.path.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.path));
            if(e.name_file.toLowerCase().includes(r.toLowerCase()) || e.name_file.toUpperCase().includes(r.toUpperCase()) || e.name_file.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.name_file));
          });
        }
        else if (t === "file" ){
          res.forEach(function(e){
            if(e.name_file.toLowerCase().includes(r.toLowerCase()) || e.name_file.toUpperCase().includes(r.toUpperCase()) || e.name_file.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.name_file));
          });
        }
        else if ( t === "database" ){
          if($('tbody#table-body').children().length !== 1 && $('tbody#table-body').children()[0].querySelectorAll('td')[0].innerHTML !== "กรุณาเลือก");
          res.forEach(function(e){
            Object.keys(e).forEach(function(f){
              let a = e[f].match(/[-:]/g)||[].length;
              if (a.length === 'undefined' || a.length !== 4){
                if(e[f] !== null && e[f].toLowerCase().includes(r.toLowerCase()) || e[f].toUpperCase().includes(r.toUpperCase()) || e[f].indexOf(r) !== -1 && containsOnlyNumbers(e[f]) === false) $('datalist#browers').append(new Option('', e[f]));
              }
            })
          })
        }
        else if( t === "sniffer" ){
          if($('tbody#table-body').children().length !== 1 && $('tbody#table-body').children()[0].querySelectorAll('td')[0].innerHTML !== "กรุณาเลือกอุปกรณ์"){
            res.forEach(function(e){
              if(e.toLowerCase().includes(r.toLowerCase()) || e.toUpperCase().includes(r.toUpperCase()) || e.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e));

            })
          }
        }
        else if( t === "manage" ){
          res.forEach(function(e){
            if(e.agm_name.toLowerCase().includes(r.toLowerCase()) || e.agm_name.toUpperCase().includes(r.toUpperCase()) || e.agm_name.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.agm_name));
            if (checkSameInner($('datalist#browers').children(), 0) === false){
              if(e.code.toLowerCase().includes(r.toLowerCase()) || e.code.toUpperCase().includes(r.toUpperCase()) || e.code.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.code));
              if(e.name.toLowerCase().includes(r.toLowerCase()) || e.name.toUpperCase().includes(r.toUpperCase()) || e.name.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.name));
              if(e.fullname.toLowerCase().includes(r.toLowerCase()) || e.fullname.toUpperCase().includes(r.toUpperCase()) || e.fullname.indexOf(r) !== -1) $('datalist#browers').append(new Option('', e.fullname));
            }
          })
        }
      }
    }else{
      $('datalist#browers').empty();
    }
  });
}
// Create pagination
const paginationCreate = (s, l) =>{
  let pages = (typeof l !== 'number') ? Math.ceil(parseInt(l.count) / parseInt(s.rows)) : Math.ceil(parseInt(l) / parseInt(s.rows));
  let wrapper = document.getElementById('pagination-wrapper');
  wrapper.innerHTML = ``
  let maxLeft = (parseInt(s.page) - Math.floor(parseInt(s.window) / 2));
  let maxRight = (parseInt(s.page) + Math.floor(parseInt(s.window) / 2));
  if (maxLeft < 1) {
    maxLeft = 1;
    maxRight = parseInt(s.window);
  }
  if (maxRight > pages) {
    maxLeft = pages - (parseInt(s.window) - 1)

    if (maxLeft < 1) {
      maxLeft = 1
    }
    maxRight = pages;
  }
  for (let page = maxLeft; page <= maxRight; page++) {
    if(page !== parseInt(s.page)){
      wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}> ${page}</button></li>`
    }else{
      wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page} disabled>${page}</button></li>`
    }
  }
  if (parseInt(s.page) === 1) {
    wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
  } else {
    wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
  }
  if (parseInt(s.page) !== pages) {
    wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link">ถัดไป &#187;</button>`
  } else {
    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link">ถัดไป &#187;</button>`
  }
  $('button.page').on('click', function () { // Not sure
    $('tbody#table-body').empty()
    s.page = Number($(this).val())
    s.querySet = []
    if (typeof s.option === 'undefined' && typeof s.select === 'undefined' && typeof s.key === 'undefined'){
      buildTable(s, 0)
    }else{
      selectSearch(s)
    } 
  })
}
// Create table
const buildTable = (state, option) => {
  let body = $('tbody#table-body');
  if(typeof state.option === 'undefined' && typeof state.select === 'undefined' && typeof state.key === 'undefined'){
    $.post(`/${subfolder}agent`, {'value': "@lltr@@gentT@b1e", 'state': state}).done(function(result){
      const s = result.state;
      const l = (typeof result.len !== 'number') ? result.len[0]:result.len
      if (s.querySet.length === 0) {
        body.append(`
        <tr>
          <td class="text-center" colspan="12" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>
        </tr>
        `)
        $('span#start').html(0)
        $('span#end').html(0)
        $('span#total').html(0)
      }else{
        for (const i in s.querySet){
          if (document.getElementById('a-index')){
            let check = ""
            if(s.querySet[i].agm_status == 1){
              check = "checked"
            }
            const last_log = (g) => {
              if(g !== null){
                let date = new Date(g)
                let time = g.split("T").pop().split(":")
                let second = time[time.length - 1].split(".")
                return ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear()+" "+time[0]+":"+time[1]+":"+second[0]
              }
              return '<span style="color: red;"> - <span>'
            }
            body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อกำกับการใช้งาน</b> <span class="tablesaw-cell-content"> ${s.querySet[i].agm_name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">รหัสของ Agent</b> <span class="tablesaw-cell-content"> ${s.querySet[i].code} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อของ Agent</b> <span class="tablesaw-cell-content"> ${s.querySet[i].name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content"> ${convert_datetime(new Date(s.querySet[i].agm_created), s.querySet[i].agm_created)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content"> ${s.querySet[i].fullname} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">สถานะ</b> <span class="tablesaw-cell-content"> ${last_log(s.querySet[i].last_access)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">เปิด/ปิดการใช้งาน</b> <span class="tablesaw-cell-content" > <div class=" form-check form-switch"> <input type="checkbox" name="agm_status" class="form-check-input" id="turnOffOn" data-value="${s.querySet[i].agm_id}" ${check} /></div>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ดูข้อมูล</b> <span class="tablesaw-cell-content"><a href="/${subfolder}detail_agent_manage/${s.querySet[i].agm_id}/" class="text-info" ><i class="fas fa-file-alt fa-2x"></i></a></span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">แก้ไขข้อมูล</b> <span class="tablesaw-cell-content"><a href="/${subfolder}edit_agent_manage/${s.querySet[i].agm_id}/" class="text-warning" ><i class="fas fa-pencil-alt fa-2x"></i></a></span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลบช้อมูล</b> <span class="tablesaw-cell-content"><a href="#delete_agent" data-value="${s.querySet[i].agm_id}" data-index="${s.querySet[i].ags_id}" class="text-danger" data-bs-toggle="modal" ><i class="fas fa-trash-alt fa-2x"></i></a></span></td>
</tr>
`)
            $('input:checkbox[name="agm_status"]').on('click', function(){
              $("div#alert-loading").css('display', 'flex');
              let value = $(this).attr('data-value');
              if($(this).prop('checked')){
                $.post(`/${subfolder}agent_manage/update/status${value}`,{value: "@lltr@@gentUp@teSt@tusM@n@ge", status: 1}).done(function(_){
                  window.location.replace = `/${subfolder}agent_manage/`;
                  $("div#alert-loading").css('display', 'none');
                });
              }else{
                $.post(`/${subfolder}agent_manage/update/status${value}`,{value: "@lltr@@gentUp@teSt@tusM@n@ge", status: 0}).done(function(_){
                  $("div#alert-loading").css('display', 'flex');
                  window.location.replace = `/${subfolder}agent_manage/`;
                  $("div#alert-loading").css('display', 'none');
                });
              }
            })
            $('a[href="#delete_agent"]').on('click',function(){
              let id = $(this).attr('data-value'); 
              let index = $(this).attr('data-index');
              $.post(`/${subfolder}agent_manage/selectManage`,{id: id, value: "@lltr@@gentSe1ectM@n@ge"}).done(function(result){
                let res = result.manage;
                let fullname = result.account.filter(i => i.acc_id == res[0].acc_id)
                if(res.length > 0){
                  $('span#agent_manage_name').html(res[0].agm_name)
                  $('span#code_agent').html(res[0].code);
                  $('span#name_agent').html(res[0].name);
                  $('span#some_createdate').html(convert_datetime(new Date(res[0].agm_created), res[0].agm_created));
                  $('span#some_creator').html(fullname[0].firstname+" "+fullname[0].lastname);
                }
              })
              $('button#button_delete_agent').on('click', function(){
                document.getElementById('form_delete_agent').action = `/${subfolder}delete_agent_manage/${id}/${index}`
                document.getElementById('form_delete_agent').submit();
              })
            })
          }else if(document.getElementById('a-store')){
            let checked = "";
            if (s.querySet[i].status !== 0){
              checked = "checked";
            }
            body.append(`
<tr>
<td> ${s.querySet[i].no} </td>
<td> <a href="/${subfolder}agent_store/detail${s.querySet[i].ags_id}">${s.querySet[i].name}</a> </td>
<td> ${s.querySet[i]._limit_} </td>
<td class="form-switch">
<input type="checkbox" name="status" ${checked} class="form-check-input" data-value="${s.querySet[i].ags_id}" />
</td>
</tr>
`)
            $('input:checkbox[name="status"]').on('click', function(){ 
              $('div#alert-loading').css({'display': 'flex'});
              if($(this).prop('checked')){
                $.post(`/${subfolder}agent_store/update`+$(this).attr('data-value'),{value: "@lltr@@gentUp@teSt@tusStore", status: 1}).done(function(){
                  window.location.replace = `/${subfolder}agent_store/`;
                  $('div#alert-loading').css({'display': 'none'});
                })
              }else{
                $.post(`/${subfolder}agent_store/update`+$(this).attr('data-value'),{value: "@lltr@@gentUp@teSt@tusStore", status: 0}).done(function(){
                  window.location.replace = `/${subfolder}agent_store`;
                  $('div#alert-loading').css({'display': 'none'});
                })
              }
            })
          }else if(document.getElementById('file_log_ag')){
            $('span#total-file').html(convert_size(result.total))
            let name = (f) =>{
              if(f.slice(-4) === "xlsx" || f.slice(-4) === "evtx"){
                return f.slice(0, -5)
              }
              return f.slice(0, -4)
            }
            let extension = (f) =>{
              if(f.slice(-4) === "xlsx" || f.slice(-4) === 'evtx'){
                return f.slice(-5)
              }
              return f.slice(-4)
            }
            body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อไฟล์: </b> <span class="tablesaw-cell-content"> ${name(s.querySet[i].name_file)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">นามสกุลไฟล์: </b> <span class="tablesaw-cell-content"> ${extension(s.querySet[i].name_file)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ขนาดไฟล์: </b> <span class="tablesaw-cell-content"> ${convert_size(s.querySet[i].size)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">เวลา/วันที่ แก้ไขล่าสุด: </b> <span class="tablesaw-cell-content"> ${convert_datetime(new Date(s.querySet[i]._get), s.querySet[i]._get)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ดูข้อมูล: </b> <span class="tablesaw-cell-content"><a href="#file_log_detail" data-bs-toggle="modal" class="text-info" data-value="${s.querySet[i].id}"><i class="fas fa-file-alt fa-2x"></i></a></span></td>
</tr>
`)
            $('a[href="#file_log_detail"]').on('click', function(){
              $.post(`/${subfolder}file_log_ag/detail`,{id: $(this).attr('data-value')}).done(function(result){
                $('span#file_name').html(result[0].name_file);
                if (result[0].name_file.slice(-4).includes(".")){
                  $('span#file_extension').html(result[0].name_file.slice(-4));
                }else{
                  $('span#file_extension').html(result[0].name_file.slice(-5));
                } 
                $('span#file_size').html(convert_size(result[0].size));
                $('span#file_edit_date').html(convert_datetime(new Date(result[0]._get), result[0]._get));
})
            })
          }else if(document.getElementById('database_ag')){
            $('div#before-table').css({"min-height": "470px", "margin-left": "0.5%", "margin-right": "1%", "overflow-x": "auto"});
            const thead = $('tr#tr_header').children().slice(1 ,$('tr#tr_header').children().length-2);
            const tdContent = (d, t, j, a) => {
              if (j === t.length){
                return a
              }else{
                if(d[j] !== null && new Date(d[j] !== 'Invalid Date') && !isNaN(new Date(d[j]))){
                  if(d[j].includes('-') && d[j].includes(':')){
                    a.push(`
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">${t[j].textContent}: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${convert_date(new Date(d[j]))} </span></td>
`)
                  }else{
                    a.push(`
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">${t[j].textContent}: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${d[j]} </span></td>
`)
                  }
                }else{
                  a.push(`
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">${t[j].textContent}: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${d[j]} </span></td>
`)
                }
                return tdContent(d, t, (j+1), a)
              }
            } 
            let raw = Object.values(s.querySet[i]);
            raw.shift(), raw.pop(), raw.pop()
            body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${s.querySet[i].no} </span></td>
${tdContent(raw, thead, 0, []).join("")}
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">วันที่/เวลาที่บันทึก: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${convert_datetime(new Date(s.querySet[i]._get), s.querySet[i]._get)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลบช้อมูล: </b> <span class="tablesaw-cell-content"><a href="#" id="${s.querySet[i].id}&&${s.querySet[i].no}" onclick="_delete(this.id)" class="text-danger" data-bs-toggle="modal" data-bs-target="#delete"><i class="fas fa-trash-alt fa-2x"></i></a></span></td>
</tr>
`)
            $('button#button_delete_db').on('click',function(){
              document.getElementById('form_db_delete').action = `/${subfolder}database_ag/delete`+$(this).attr('data-value')
              document.getElementById('form_db_delete').submit();
            })
          }else if(document.getElementById('logger_hash_ag')){
            body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่ออุปกรณ์ที่ส่งมา</b> <span class="tablesaw-cell-content"> ${s.querySet[i].device_name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ระบบปฏิบัติการ</b> <span class="tablesaw-cell-content"> ${s.querySet[i].os_name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ที่อยู่ของไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].path} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].name_file} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">รายละเอียด</b> <span class="tablesaw-cell-content"><a id="${s.querySet[i].id}" href="javascript:void(0)" onclick="detail(this.id)" class="text-info" data-bs-toggle="modal" data-bs-target="#detail-hash"><i class="fas fa-file-alt fa-2x"></i></a></span></td>
</tr>
`)
          }else if(document.getElementById('a-sniffer')){
            body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">สกุลไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].extension} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ขนาดของไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].size} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">รายละเอียด</b> <span class="tablesaw-cell-content"><a href="#detail_file" data-value="${s.querySet[i].name}${s.querySet[i].extension}" class="text-info" data-bs-toggle="modal" ><i class="fas fa-file-alt fa-2x"></i></a></span></td>
</tr>
`)
            $('a[href="#detail_file"]').on('click', function(){
              let value = $(this).attr('data-value')
              $('span#detail-name').text(value)
              $('span#inside-name').text(value)
              $.post(`/${subfolder}agent_sniffer/select`,{'value': "@lltr@@gentSn1ffer", 'from': s.from+value}).done(function(result){
                $('textarea#detail-show').text(result)
              })
            })
          }
        }
        $('span#start').html(s.querySet[0].no);
        $('span#end').html(s.querySet[s.querySet.length - 1].no);
        $('span#total').html((typeof l !== 'number') ? l.count:l);
        paginationCreate(s, l);
      }
    })
  }else{ // Today 2023/01/13 not have search agent store.
    const s = state;
    const l = (typeof option !== 'number') ? option[0]:option
    body.empty(); // or $('tbody#table-body tr').remove();
    if (s.querySet.length === 0) {
      body.append(`<tr><td class="text-center" colspan="12" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td></tr>`)
      $('span#start').html(0)
      $('span#end').html(0)
      $('span#total').html(0)
    }else{
      for (const i in s.querySet){
        if (document.getElementById('a-index')){
          let check = ""
          if(s.querySet[i].agm_status == 1){
            check = "checked"
          }
          const last_log = (g) => {
            if(g !== null){
              let date = new Date(g)
              let time = g.split("T").pop().split(":")
              let second = time[time.length - 1].split(".")
              return ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear()+" "+time[0]+":"+time[1]+":"+second[0]
            }
            return '<span style="color: red;"> - <span>'
          }
          body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อกำกับการใช้งาน</b> <span class="tablesaw-cell-content"> ${s.querySet[i].agm_name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">รหัสของ Agent</b> <span class="tablesaw-cell-content"> ${s.querySet[i].code} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อของ Agent</b> <span class="tablesaw-cell-content"> ${s.querySet[i].name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content"> ${convert_datetime(new Date(s.querySet[i].agm_created), s.querySet[i].agm_created)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content"> ${s.querySet[i].fullname} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">สถานะ</b> <span class="tablesaw-cell-content"> ${last_log(s.querySet[i].last_access)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">เปิด/ปิดการใช้งาน</b> <span class="tablesaw-cell-content" > <div class=" form-check form-switch"> <input type="checkbox" name="agm_status" class="form-check-input" id="turnOffOn" data-value="${s.querySet[i].agm_id}" ${check} /></div>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ดูข้อมูล</b> <span class="tablesaw-cell-content"><a href="/${subfolder}detail_agent_manage/${s.querySet[i].agm_id}/" class="text-info" ><i class="fas fa-file-alt fa-2x"></i></a></span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">แก้ไขข้อมูล</b> <span class="tablesaw-cell-content"><a href="/${subfolder}edit_agent_manage/${s.querySet[i].agm_id}/" class="text-warning" ><i class="fas fa-pencil-alt fa-2x"></i></a></span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลบช้อมูล</b> <span class="tablesaw-cell-content"><a href="#delete_agent" data-value="${s.querySet[i].agm_id}" data-index="${s.querySet[i].ags_id}" class="text-danger" data-bs-toggle="modal" ><i class="fas fa-trash-alt fa-2x"></i></a></span></td>
</tr>
`)
          $('input:checkbox[name="agm_status"]').on('click', function(){
            $("div#alert-loading").css('display', 'flex');
            let value = $(this).attr('data-value');
            if($(this).prop('checked')){
              $.post(`/${subfolder}agent_manage/update/status${value}`,{value: "@lltr@@gentUp@teSt@tusM@n@ge", status: 1}).done(function(_){
                window.location.replace = `/${subfolder}agent_manage/`;
                $("div#alert-loading").css('display', 'none');
              });
            }else{
              $.post(`/${subfolder}agent_manage/update/status${value}`,{value: "@lltr@@gentUp@teSt@tusM@n@ge", status: 0}).done(function(_){
                $("div#alert-loading").css('display', 'flex');
                window.location.replace = `/${subfolder}agent_manage/`;
                $("div#alert-loading").css('display', 'none');
              });
            }
          })
          $('a[href="#delete_agent"]').on('click',function(){
            let id = $(this).attr('data-value'); 
            let index = $(this).attr('data-index');
            $.post(`/${subfolder}agent_manage/selectManage`,{id: id, value: "@lltr@@gentSe1ectM@n@ge"}).done(function(result){
              let res = result.manage;
              let fullname = result.account.filter(i => i.acc_id == res[0].acc_id)
              if(res.length > 0){
                $('span#agent_manage_name').html(res[0].agm_name)
                $('span#code_agent').html(res[0].code);
                $('span#name_agent').html(res[0].name);
                $('span#some_createdate').html(convert_datetime(new Date(res[0].agm_created), res[0].agm_created));
                $('span#some_creator').html(fullname[0].firstname+" "+fullname[0].lastname);
              }
            })
            $('button#button_delete_agent').on('click', function(){
              document.getElementById('form_delete_agent').action = `/delete_agent_manage/${id}/${index}`
              document.getElementById('form_delete_agent').submit();
            })
          })
        }else if(document.getElementById('file_log_ag')){
          $('span#total-file').html(convert_size(state.option))
          let name = (f) =>{
            if(f.slice(-4) === "xlsx" || f.slice(-4) === "evtx"){
              return f.slice(0, -5)
            }
            return f.slice(0, -4)
          }
          let extension = (f) =>{
            if(f.slice(-4) === "xlsx" || f.slice(-4) === 'evtx'){
              return f.slice(-5)
            }
            return f.slice(-4)
          }
          body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อไฟล์: </b> <span class="tablesaw-cell-content"> ${name(s.querySet[i].name_file)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">นามสกุลไฟล์: </b> <span class="tablesaw-cell-content"> ${extension(s.querySet[i].name_file)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ขนาดไฟล์: </b> <span class="tablesaw-cell-content"> ${convert_size(s.querySet[i].size)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">เวลา/วันที่ แก้ไขล่าสุด: </b> <span class="tablesaw-cell-content"> ${convert_datetime(new Date(s.querySet[i]._get), s.querySet[i]._get)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ดูข้อมูล: </b> <span class="tablesaw-cell-content"><a href="#file_log_detail" data-bs-toggle="modal" class="text-info" data-value="${s.querySet[i].id}"><i class="fas fa-file-alt fa-2x"></i></a></span></td>
</tr>
`)
          $('a[href="#file_log_detail"]').on('click', function(){
            $.post(`/${subfolder}file_log_ag/detail`,{id: $(this).attr('data-value')}).done(function(result){
              $('span#file_name').html(result[0].name_file);
              if (result[0].name_file.slice(-4).includes(".")){
                $('span#file_extension').html(result[0].name_file.slice(-4));
              }else{
                $('span#file_extension').html(result[0].name_file.slice(-5));
              } 
              $('span#file_size').html(convert_size(result[0].size));
              $('span#file_edit_date').html(convert_datetime(new Date(result[0]._get), result[0]._get));
            })
          })
        }else if(document.getElementById('database_ag')){
          $('div#before-table').css({"min-height": "470px", "margin-left": "0.5%", "margin-right": "1%", "overflow-x": "auto"});
          const thead = $('tr#tr_header').children().slice(1 ,$('tr#tr_header').children().length-2);
          const tdContent = (d, t, j, a) => {
            if (j === t.length){
              return a
            }else{
              if(typeof d[j] !== 'number' && d[j] !== null && new Date(d[j] !== 'Invalid Date') && !isNaN(new Date(d[j]))){
                if(d[j].includes('-') && d[j].includes(':')){
                  a.push(`
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">${t[j].textContent}: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${convert_date(new Date(d[j]))} </span></td>`)
                }else{
                  a.push(`<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">${t[j].textContent}: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${d[j]} </span></td>`)
                }
              }else{
                a.push(`<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">${t[j].textContent}: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${d[j]} </span></td>`)
              }
              return tdContent(d, t, (j+1), a)
            }
          } 
          let raw = Object.values(s.querySet[i]);
          raw.shift(), raw.pop(), raw.pop()
          body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${s.querySet[i].no} </span></td>
${tdContent(raw, thead, 0, []).join("")}
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">วันที่/เวลาที่บันทึก: </b> <span class="tablesaw-cell-content" style="overflow: auto;"> ${convert_datetime(new Date(s.querySet[i]._get), s.querySet[i]._get)} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลบช้อมูล: </b> <span class="tablesaw-cell-content"><a href="#" id="${s.querySet[i].id}&&${s.querySet[i].no}" onclick="_delete(this.id)" class="text-danger" data-bs-toggle="modal" data-bs-target="#delete"><i class="fas fa-trash-alt fa-2x"></i></a></span></td>
</tr>
`)
          $('button#button_delete_db').on('click',function(){
            document.getElementById('form_db_delete').action = `/${subfolder}database_ag/delete`+$(this).attr('data-value')
            document.getElementById('form_db_delete').submit();
          })
        }else if(document.getElementById('logger_hash_ag')){
          body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่ออุปกรณ์ที่ส่งมา</b> <span class="tablesaw-cell-content"> ${s.querySet[i].device_name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ระบบปฏิบัติการ</b> <span class="tablesaw-cell-content"> ${s.querySet[i].os_name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ที่อยู่ของไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].path} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].name_file} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">รายละเอียด</b> <span class="tablesaw-cell-content"><a id="${s.querySet[i].id}" href="javascript:void(0)" onclick="detail(this.id)" class="text-info" data-bs-toggle="modal" data-bs-target="#detail-hash"><i class="fas fa-file-alt fa-2x"></i></a></span></td>
</tr>
`)
        }else if(document.getElementById('a-sniffer')){
          body.append(`
<tr>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ลำดับที่: </b> <span class="tablesaw-cell-content"> ${s.querySet[i].no} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ชื่อไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].name} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">สกุลไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].extension} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">ขนาดของไฟล์</b> <span class="tablesaw-cell-content"> ${s.querySet[i].size} </span></td>
<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist" ><b class="tablesaw-cell-label">รายละเอียด</b> <span class="tablesaw-cell-content"><a href="#detail_file" data-value="${s.querySet[i].name}${s.querySet[i].extension}" class="text-info" data-bs-toggle="modal" ><i class="fas fa-file-alt fa-2x"></i></a></span></td>
</tr>
`)
          $('a[href="#detail_file"]').on('click', function(){
            let value = $(this).attr('data-value')
            $('span#detail-name').text(value)
            $('span#inside-name').text(value)
            $.post(`/${subfolder}agent_sniffer/select`,{'value': "@lltr@@gentSn1ffer", 'from': s.from+value}).done(function(result){
              $('textarea#detail-show').text(result)
            })
          })
        }
      }
      $('span#start').html(s.querySet[0].no);
      $('span#end').html(s.querySet[s.querySet.length - 1].no);
      $('span#total').html((typeof l !== 'number') ? l.count:l);
      paginationCreate(s, l);
    }
  }
}
//Select data for input search
const selectSearch = (s) =>{ 
  if ($('tbody#table-body').children().length !== 0){
    if( (s.type === "database" && $('tbody#table-body').children().length === 1 && $('tbody#table-body').children()[0].querySelectorAll('td')[0].innerHTML === "กรุณาเลือก") || $('tbody#table-body').children()[0].querySelectorAll('td')[0].querySelectorAll('span')[0].innerHTML === "กรุณาเลือกอุปกรณ์"){
      $('div#err-search').css({'overflow-y': "auto", 'display': ""});
      $('span#content-err').html('ค้นหาข้อมูล');
      $('button:button#btn-err').on('click',function(){
        $('div#err-search').css('display', 'none');
      });
    }
  }
  $.post(`/${subfolder}agent/getSearch`,{ value: "@lltr@@gentSe1ectSe@rchR@w", state: s}).done(function(res){
    buildTable(res.state, res.len)
  });
}
// Function from agent manage new database
const addTable = () =>{
  const manuallyAdd = (val) => {
    $('div#subject-table').append(`
<div class="form-check">
<input class="form-check-input" type="radio" name="selectTable" value="${val}" id="table-${val}" checked>
<label class="form-check-label" for="table-${val}">
${val}
</label><span style="float: right; margin-right: 40%;"><span id="buttonDisplayColumns" data-value="${val}" data-count="0" onclick="displayColumns(this)" ><i class="fas fa-angle-right"></i></span>&emsp;|&emsp;<span id="addColumn" data-value="${val}" onclick="addColumn(this)"><i class="fas fa-plus"></i></span>&emsp;|&emsp;<span id="delColumn" data-value="${val}" onclick="delTable(this)"><i class="fas fa-minus"></i></span></span><br/>
<div id="column-${val}">
</div>
</div>
`)
  }
  const uncheck = (c, i) => {
    if(typeof c === 'object'){
      if(i === c.length){
        return -1
      }else{
        document.querySelector(`span#buttonDisplayColumns[data-value="${c[i].getAttribute('id').split("-")[1]}"]`).setAttribute('data-count', 0)
        document.querySelector(`span#buttonDisplayColumns[data-value="${c[i].getAttribute('id').split("-")[1]}"]`).innerHTML = '<i class="fas fa-angle-right"></i>'
        $(`input[id*="col-${c[i].getAttribute('id').split("-")[1]}"]`).map(e => $(`input[id*="col-${c[i].getAttribute('id').split("-")[1]}"]`)[e].removeAttribute('checked'));
        $(`div#displayColumns-${c[i].getAttribute('id').split("-")[1]}`).map(e => $(`div#displayColumns-${c[i].getAttribute('id').split("-")[1]}`)[e].style.display = "none");
        return uncheck(c, (i+1))
      }
    }else{
      document.querySelector(`span#buttonDisplayColumns[data-value="${c}"]`).setAttribute('data-count', 0)
      document.querySelector(`span#buttonDisplayColumns[data-value="${c}`).innerHTML = '<i class="fas fa-angle-right"></i>'
      $(`input[id="col-${c}"]`).map(e => $(`input[id="col-${c[i].getAttribute('id').split("-")[1]}"]`)[e].removeAttribute('checked'));
      $(`div#displayColumns-${c}`).map(e => $(`div#displayColumns-${c[i].getAttribute('id').split("-")[1]}`)[e].style.display = "none");
    }
  }
  if ($('input[name="table"]').val() !== "" && document.querySelectorAll(`div[id*="column-"]`).length == 0){
    manuallyAdd($('input[name="table"]').val());
  }else if($('input[name="table"]').val() !== "" && document.querySelectorAll(`div[id*="column-"]`).length > 0){
    uncheck(document.querySelectorAll(`div[id*="column-"]`), 0);
    manuallyAdd($('input[name="table"]').val());
  }
  ($('div#subject-table').children().length > 1 && ($('div#subject-table').children()[0].tagName === "SPAN")) ? $('div#subject-table').children()[0].remove():-1
  $('input[name="table"]').val("")
  let bkup0 = document.querySelector('input[name="selectTable"]:checked').value
  $('input:radio[name="selectTable"]').on('click', function(_){
    if($('input:radio[name="selectTable"]').length != 1){
      uncheck(document.querySelectorAll(`div[id="column-${bkup0}"]`), 0);
      document.querySelector(`span#buttonDisplayColumns[data-value="${$(this).val()}"]`).setAttribute('data-count', 1)
      document.querySelector(`span#buttonDisplayColumns[data-value="${$(this).val()}"]`).innerHTML = '<i class="fas fa-angle-down"></i>'
      document.querySelectorAll(`div#displayColumns-${$(this).val()}`).forEach(e => e.style.display = "block");
      for (const i in document.querySelector(`div#column-${$(this).val()}`).children){
        if(!isNaN(Number(i))){
          document.querySelector(`div#column-${$(this).val()}`).children[i].querySelector(`input[name="${$(this).val()}_selectCol_${i}"]`).setAttribute('checked', true)
        }
      }
      bkup0 = $(this).val()
    }
  })
}
// Function from agent manage new database
const delTable = (s) =>{
  $('span#table-name').html(s.getAttribute('data-value'));
  $('div#alert-confirm').css('display', 'flex');
  $('button:button').on('click', function(_){
    if($(this).attr('id') === 'alert-confirm-yes'){
      s.parentNode.parentNode.remove();
      $('div#alert-confirm').css('display', 'none');
    }else{
      $('div#alert-confirm').css('display', 'none');
    }
    ($('div#subject-table').children().length === 0) ? $('div#subject-table').html('<span style="color: red;">ไม่มีข้อมูล, ทดสอบการเชื่อมต่อ หรือ เพิ่ม Table ขึ้นมา </span>'):-1
  })
}
// Function from agent manage new database
const displayColumns = (r) =>{
  if(Number(r.getAttribute('data-count')) === 1){
    r.innerHTML = '<i class="fas fa-angle-right"></i>'
    $(`div#displayColumns-${r.getAttribute('data-value')}`).css('display', 'none');
  }else{
    r.innerHTML = '<i class="fas fa-angle-down"></i>'
    $(`div#displayColumns-${r.getAttribute('data-value')}`).css('display', 'block');
  }
  (Number(r.getAttribute('data-count')) === 0) ? r.setAttribute('data-count', 1):r.setAttribute('data-count', 0)
}
// Function from agent manage new database
const appendColumns = (n, c) => { // Today can't many table 31/01/2023
  if($('input[name="selectTable"]').is('checked')){
    for(let i in c){
      $(`div#column-${n}`).append(`
<div id="displayColumns-${n}" class="form-check" style="display: block;">
<input class="form-check-input" type="checkbox" name="${n}_selectCol_${i}" value="${c[i]}" id="col-${n}-${i}" checked>
<label class="form-check-label" for="col-${n}-${i}">
${c[i]}
</label><span id="delColumn" data-value="${n}" data-num="${n}-${i}" onclick="delColumn(this)" style="float: right; margin-right: 52.5%; color: red;"><i class="fas fa-minus"></i></span>
</div>
`)
    }
  }else{
    for(let i in c){
      $(`div#column-${n}`).append(`
<div id="displayColumns-${n}" class="form-check" style="display: none;">
<input class="form-check-input" type="checkbox" name="${n}_selectCol_${i}" value="${c[i]}" id="col-${n}-${i}">
<label class="form-check-label" for="col-${n}-${i}">
${c[i]}
</label><span id="delColumn" data-value="${n}" data-num="${n}-${i}" onclick="delColumn(this)" style="float: right; margin-right: 52.5%; color: red;"><i class="fas fa-minus"></i></span>
</div>
`)
    }
  }
}
// Function from agent manage new database
const appendTable = (n, c) => {
  $('div#subject-table').append(`
<div class="form-check">
<input class="form-check-input" type="radio" name="selectTable" value="${n}" id="table-${n}">
<label class="form-check-label" for="table-${n}">
${n}
</label><span style="float: right; margin-right: 40%;"><span id="buttonDisplayColumns" data-value="${n}" data-count="0" onclick="displayColumns(this)" ><i class="fas fa-angle-right"></i></span>&emsp;|&emsp;<span id="addColumn" data-value="${n}" onclick="addColumn(this)"><i class="fas fa-plus"></i></span>&emsp;|&emsp;<span id="delColumn" data-value="${n}" onclick="delTable(this)"><i class="fas fa-minus"></i></span></span><br/>
<div id="column-${n}">
</div>
</div>
`)
  appendColumns(n, c)
}
// Function from agent manage new database
const addColumn = (s) =>{
  let val = s.getAttribute('data-value');
  if(document.querySelector(`div#displayColumns-${val}`) === null){
    document.querySelector(`span#buttonDisplayColumns[data-value="${val}"]`).innerHTML = '<i class="fas fa-angle-down"></i>'
    document.querySelector(`span#buttonDisplayColumns[data-value="${val}"]`).setAttribute('data-count', 1)
    $(`div#column-${val}`).append(`
<div id="displayColumns-${val}" class="form-check" style="display: block;">
<input class="form-check-input" type="checkbox" name="${val}_selectCol_${$(`div#column-${val}`).children().length}" value="" id="col-${val}-${$(`div#column-${val}`).children().length}" checked>
<label class="form-check-label" for="col-${val}-${$(`div#column-${val}`).children().length}">
<input class="form-control" id="newCol" data-num="${val}-${$(`div#column-${val}`).children().length}" type="text" style="height: 25px;"/>
</label><span id="delColumn" data-value="${val}" data-num="${val}-${$(`div#column-${val}`).children().length}" onclick="delColumn(this)" style="float: right; margin-right: 52.5%; color: red;"><i class="fas fa-minus"></i></span>
</div>
`)
  }else{
    if(document.querySelector(`div#displayColumns-${val}`).style.display === "none"){
      displayColumns(document.querySelector(`span[data-value="${val}"]`))
    }
    $(`div#column-${val}`).append(`
<div id="displayColumns-${val}" class="form-check" style="display: block;">
<input class="form-check-input" type="checkbox" name="${val}_selectCol_${$(`div#column-${val}`).children().length}" value="" id="col-${val}-${$(`div#column-${val}`).children().length}" checked>
<label class="form-check-label" for="col-${val}-${$(`div#column-${val}`).children().length}">
<input class="form-control" id="newCol" data-num="${val}-${$(`div#column-${val}`).children().length}" type="text" style="height: 25px;"/>
</label><span id="delColumn" data-value="${val}" data-num="${val}-${$(`div#column-${val}`).children().length}" onclick="delColumn(this)" style="float: right; margin-right: 52.5%; color: red;"><i class="fas fa-minus"></i></span>
</div>
`)
  }
  $('input#newCol').on('keyup keydown change focus', function(_){
    $(this).parents().find(`input:checkbox#col-${$(this).attr('data-num')}`).val($(this).val())
  })
}
// Function from agent manage new database
const delColumn = (s) =>{
  if($(`div#displayColumns-${s.getAttribute('data-value')}`).length > 1){
    $('span#column-name-modal').html(s.parentNode.children[0].value)
    $('div#alert-confirm-column').css('display', 'flex'); 
    $('button:button').on('click', function(_){
      if ($(this).attr('id') === "alert-confirm-column-yes"){
        $(`input:checkbox#col-${s.getAttribute('data-num')}`).parent().remove()
        $('div#alert-confirm-column').css('display', 'none')
      }else{
        ($(this).attr('id') === "alert-confirm-column-no") ? $('div#alert-confirm-column').css('display', 'none'):-1;
      }
    })
  }else{
    $('div#alert-column').css('display', "flex");
    $('button:button').on('click', function(_){
      ($(this).attr('id') === "alert-column") ? $('div#alert-column').css('display', "none"):-1;
    })
  }
}
//Add details new and edit agent manage.
const detailConfig = (t, n, k) => {
  let body = $('div#detail-config');
  if(t === 'AG1'){
    if(k !== null){
      body.html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> ชนิดของ Hash </th>
<td style="text-align: left;">
<span style="color: red;">${k.hash_name}</span>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
ระบุที่อยู่ไฟล์จราจร
</th>
<td style="tet-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: ./work/ หรือ /home/user/work/ หรือ C:\\Users\\Admin\\Log\\
</div>
<input type="text" name="total_path" value="${k.old_value}," readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
    }else{
      body.html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> ชนิดของ Hash </th>
<td style="text-align: left;">
<div class="row">
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="hash_type" id="type1" value="-1" class="form-check-input" required="">
<label for="type1" class="form-check-label"> MD5 </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="hash_type" id="type2" value="0" class="form-check-input" required="">
<label for="type2" class="form-check-label"> SHA-1 </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="hash_type" id="type3" value="1" class="form-check-input" required="" checked>
<label for="type3" class="form-check-label"> SHA-256 </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
ระบุที่อยู่ไฟล์จราจร
</th>
<td style="text-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: /home/user/work/ หรือ C:\\Users\\Admin\\Log\\
</div>
<input type="text" name="total_path" readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
      $('div#show-tags-path').html('<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>')
    }
  }else if(t === 'AG2'){
    if(k !== null){
      body.html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
เลือกรูปแบบที่ต้องการ
</th>
<td style="tet-align: left;">
<div class="row">
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="type_of_path" id="type1" value="-1" class="form-check-input" ${k.check[0]} required/>
<label for="type1" class="form-check-label"> เฉพาะไฟล์ๆเดียว </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-3">
<div class="form-check form-check-inline">
<input type="radio" name="type_of_path" id="type2" value="0" class="form-check-input" ${k.check[1]} required/>
<label for="type2" class="form-check-label"> เฉพาะสกุลไฟล์ที่เลือก </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="type_of_path" id="type3" value="1" class="form-check-input" ${k.check[2]} required/>
<label for="type3" class="form-check-label"> ไฟล์ทั้งหมดในที่อยู่ </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
</div>
</td>
</table>
<div id="subject-content"></div>
`)
    }else{
      body.html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
เลือกรูปแบบที่ต้องการ
</th>
<td style="tet-align: left;">
<div class="row">
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="type_of_path" id="type1" value="-1" class="form-check-input" required/>
<label for="type1" class="form-check-label"> เฉพาะไฟล์ๆเดียว </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-3">
<div class="form-check form-check-inline">
<input type="radio" name="type_of_path" id="type2" value="0" class="form-check-input" required/>
<label for="type2" class="form-check-label"> เฉพาะสกุลไฟล์ที่เลือก </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-2">
<div class="form-check form-check-inline">
<input type="radio" name="type_of_path" id="type3" value="1" class="form-check-input" required/>
<label for="type3" class="form-check-label"> ไฟล์ทั้งหมดในที่อยู่ </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
</div>
</td>
</table>
<div id="subject-content"></div>
`)
    }
  }else if(t === "AG3"){
    if(k !== null){
      body.html(`
<table class="table table-boardless">
<th width="15%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> เลือก Service Database ที่คุณใช้ </th>
<td style="text-align: left;">
<div class="row">
<div class="col-md-2">
<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="service_database" id="oracle" value="0" ${k.check[0]} required/>
<label class="form-check-label" for="oracle"> Oracle DB </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-2">
<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="service_database" id="mysql" value="1" ${k.check[1]} required/>
<label class="form-check-label" for="mysql"> MySQL </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div> 
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> IP หรือ Host ของ service database </th>
<td style="text-align: left;">
<input type="text" name="host" value="${k.old_value[1]}" placeholder="กรุณาป้อนIP..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อน IP หรือ Host ของ service database!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> Username ของ service database </th>
<td style="text-align: left;">
<input type="text" name="username" value="${k.old_value[2]}" placeholder="กรุณาป้อนUser..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อน Username ของ service database!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> Password ของ service database </th>
<td style="text-align: left;">
<input type="password" name="password" value="${k.old_value[3]}" placeholder="กรุณาป้อนPass..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อน Password ของ service database!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> ชื่อของ Database ที่ต้องการของ </th>
<td style="text-align: left;">
<input type="text" name="database" value="${k.old_value[4]}" placeholder="กรุณาป้อนDatabase..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อนชื่อของ Database ที่ต้องการ!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> ทดสอบการเชื่อมต่อ </th>
<td style="text-align: left;">
<button type="button" id="testDB" class="btn btn-info"> Test connect </button>
</td>
</table>
<table id="show-tables" class="table table-boardless" style="display: block;">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> Table ของ database ที่เลือก </th>
<td style="text-align: left;">
<div class="input-group">
<input type="text" name="table" class="form-control" placeholder="กรุณาป้อนชื่อTable..." />
<button id="add-table" type="button" class="form-input-group btn btn-outline-success" onClick="addTable()"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div id="subject-table" style="margin-top:1%">
</div>
</td>
</table>
`)
      $('input[name="table"]').on('keyup keydown change focus keypress', function(e){
        if(e.which === 32 || e.which === 45){ // Block blank space and symbol (-)
          return false;
        }
      })
      if(k.table){ // Today can't many table 31/01/2023
        let tb = k.table.split(",");
        for (let j in tb){
          $('div#subject-table').append(`
<div class="form-check">
<input class="form-check-input" type="radio" name="selectTable" value="" id="table-${tb[j]}" checked>
<label class="form-check-label" for="table-${tb[j]}">
${tb[j]}
</label><span style="float: right; margin-right: 40%;"><span id="buttonDisplayColumns" data-value="${tb[j]}" data-count="0" onclick="displayColumns(this)" ><i class="fas fa-angle-right"></i></span>&emsp;|&emsp;<span id="addColumn" data-value="${tb[j]}" onclick="addColumn(this)"><i class="fas fa-plus"></i></span>&emsp;|&emsp;<span id="delColumn" data-value="${tb[j]}" onclick="delTable(this)"><i class="fas fa-minus"></i></span></span><br/>
<div id="column-${tb[j]}">
</div>
</div>
`)
          appendColumns(tb, k.col.pop())
          displayColumns(document.querySelector(`span[data-value="${tb[j]}"]`))
        }
        for (const i in document.querySelector(`div#column-${k.table}`).children){ // can't use many table 01/02/2023
          (!isNaN(Number(i))) ? document.querySelector(`div#column-${k.table}`).children[i].querySelector(`input[name="${k.table}_selectCol_${i}"]`).setAttribute('checked', true) : -1
        }
      }else{
        $('div#subject-table').append(`<span style="color: red;">ไม่มีข้อมูล, ทดสอบการเชื่อมต่อ หรือ เพิ่ม Table ขึ้นมา </span>`)
      }
    }else{
      body.html(`
<table class="table table-boardless">
<th width="15%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> เลือก Service Database ที่คุณใช้ </th>
<td style="text-align: left;">
<div class="row">
<div class="col-md-2">
<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="service_database" id="oracle" value="0" required/>
<label class="form-check-label" for="oracle"> Oracle DB </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-2">
<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="service_database" id="mysql" value="1" checked required/>
<label class="form-check-label" for="mysql"> MySQL </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div> 
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> IP หรือ Host ของ service database </th>
<td style="text-align: left;">
<input type="text" name="host" placeholder="กรุณาป้อนIP..." class="form-control" required/>
<div id="inhost" class="invalid-feedback">
กรุณาป้อน IP หรือ Host ของ service databas!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> Username ของ service database </th>
<td style="text-align: left;">
<input type="text" name="username" placeholder="กรุณาป้อนUser..." class="form-control" required/>
<div id="inusername" class="invalid-feedback">
กรุณาป้อน Username ของ service database!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> Password ของ service database </th>
<td style="text-align: left;">
<input type="password" name="password" placeholder="กรุณาป้อนPass..." class="form-control" required/>
<div id="inpassword" class="invalid-feedback">
กรุณาป้อน Password ของ service database!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> ชื่อของ Database ที่ต้องการของ </th>
<td style="text-align: left;">
<input type="text" name="database" placeholder="กรุณาป้อนDatabase..." class="form-control" required/>
<div id="indatabase" class="invalid-feedback">
กรุณาป้อน Database...
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> ทดสอบการเชื่อมต่อ </th>
<td style="text-align: left;">
<button type="button" id="testDB" class="btn btn-info"> Test connect </button>
</td>
</table>
<table id="show-tables" class="table table-boardless" style="display: block;">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> Table ของ database ที่เลือก <br/><span style="color: red; font-size: 11px;">**ได้แค่ 10 col ต่อ 1 table**</span></th>
<td style="text-align: left;">
<div class="input-group">
<input type="text" name="table" class="form-control" placeholder="กรุณาป้อนชื่อTable..." />
<button id="add-table" type="button" class="form-input-group btn btn-outline-success" onClick="addTable()"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div id="subject-table" style="margin-top:1%">
<span style="color: red;">ไม่มีข้อมูล, ทดสอบการเชื่อมต่อ หรือ เพิ่ม Table ขึ้นมา </span>
</div>
</td>
</table>
`)
      $('input[name="table"]').on('keyup keydown change focus keypress', function(e){
        if(e.which === 32 || e.which === 45){ // Block blank space and symbol (-)
          return false;
        }
      })
    }
  }else if(t === "AG4"){
    if(k !== null){
      body.html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน <span style="color: red;">ไม่รองรับ Windows </span></th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> เลือกโหมดการใช้งาน </th>
<td style="text-align: left;">
<div class="row">
<div class="col-md-1">
<div class="form-check form-check-inline">
<input type="radio" name="mode" value="0" id="mode" class="form-check-input" ${k.check[0]}/>
<label class="form-check-label" for="mode"> FTP </label>
</div>
</div>
<div class="col-md-4">
<div class="form-check form-check-inline">
<input type="radio" name="mode" value="1" id="mode" class="form-check-input" ${k.check[1]}/>
<label class="form-check-label" for="mode"> Syslogs </label>
</div>
</div>
</div>
</td>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> ระบุที่อยู่ของไฟล์เพื่อจัดเก็บ </th>
<td style="text-align: left;">
<input type="text" name="path" value="${k.old_value[1]}" placeholder="กรุณาป้อนที่อยู่..." class="form-control" /><br/>
<u>ตัวอย่าง</u>: ./sniffer/ หรือ /home/user/sniffer/
</td>
</table>
<div id="subject-mode">
</div>
`)
    }else{
      body.html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${n} เพื่อนำไปใช้งาน <span style="color: red;">ไม่รองรับ Windows </span></th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> เลือกโหมดการใช้งาน </th>
<td style="text-align: left;">
<div class="row">
<div class="col-md-1">
<div class="form-check form-check-inline">
<input type="radio" name="mode" value="0" id="mode" class="form-check-input" onclick="handclick(this.value)" checked required/>
<label class="form-check-label" for="mode"> FTP </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
<div class="col-md-4">
<div class="form-check form-check-inline">
<input type="radio" name="mode" value="1" id="mode" class="form-check-input" onclick="handclick(this.value)" required/>
<label class="form-check-label" for="mode"> Syslogs </label>
<div class="invalid-feedback">
กรุณาเลือก!!!
</div>
</div>
</div>
</div>
</td>
</table>
<div id="subject-mode">
</div>
`)
    }
  }
}
// Check input null agent new type database.
const inputCheckNull = (o) => {
  let res = (o.host !== "" && o.username !== "" && o.password !== "" && o.database !== "") ? 1:-1
  const selected = (o1, o2) => { 
    $(`div#in${o2.n}`).css('display', o2.d)
    $('input[name="'+o1.n+'"]').css({'border-color' : o1.bc, 'padding-right' : o1.pr, 'background-image': o1.bi, 'background-repeat': o1.br, 'background-position': o1.bp, 'background-size': o1.bs})
  };
  for (let i in o){
    (o[i] === "") ? selected({'n': i, 'bc': '#f62d51', 'pr': 'calc(1.5em + .75rem)', 'bi': "url(/UI/assets/images/agent/alert.svg)", 'br': 'no-repeat', 'bp': 'right calc(.375em + .1875rem) center', 'bs': 'calc(.75em + .375rem) calc(.75em + .375rem)'}, {'n': `${i}`, 'd': 'block'}):selected({'n': i, 'bc': '', 'pr': '', 'bi': "", '': '', 'bp': '', 'bs': ''}, {'n': `${i}`, 'd': 'none'})
  }
  return res
}
// Function from agent manage new database
function findMatchObject(_object){
  let query = new RegExp("err", 'i');
  let rs = Object.keys(_object).find(function(q){ return query.test(q) })
  if(typeof rs === 'undefined'){
    return false
  }else{
    return true
  }
}
// Agent manage index page.
if (document.getElementById('a-index')) {
  $('input[name="search"]').on('change keyup keydown', function(){
    searchData('manage', $(this).val().trim(), null);
  })
  // ==================== Table Show =========================
  let state = {
    'type': 'manage',
    'querySet': [],
    'page': 1,
    'rows': 10,
    'window': 10,
  }
  buildTable(state, 0);
  $('button:button#btn-search').on('click', function(){
    /* have two method 
    1). send id and get someone same id. or
    2). send same search normal input and get same all this.
    */
    let input = $('input[name="search"]');
    if (input.val() === ""){
      input.css({ 'border-stype': 'soild', 'border-color': 'red' });
    }else{
      state.select = input.val()
      selectSearch(state);
    } 
  })
  $('a#refresh-cw').on('click', function(){ //Hold
    $('input[name="search"]').val('');
    $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
    $('tbody#table-body').empty();
    $('span#start').empty();
    $('span#end').empty();
    $('span#total').empty();
    $('ul#pagination-wrapper').empty();
    delete state.select;
    buildTable(state);
  })
}
// Agent store page
else if(document.getElementById('a-store')){
  let state = {
    'type': 'store',
    'querySet': [],
    'page': 1,
    'rows': 10,
    'window': 10,
  };
  buildTable(state, 0);
}
// Agent file/dir page
else if (document.getElementById('file_log_ag')) {
  $('input[name=search]').on('change keyup keydown', function() {
    searchData('file', $(this).val().trim(), null);
  });
  var state = {
    'type': 'file',
    'querySet': [],
    'page': 1,
    'rows': 10,
    'window': 10,
  }
  buildTable(state, 0);
  $('button:button#btn-search').on('click', function(){ 
    /* have two method 
    1). send id and get someone same id. or
    2). send same search normal input and get same all this.
    */
    let input = $('input[name="search"]');
    if (input.val() === ""){
      input.css({ 'border-stype': 'soild', 'border-color': 'red' });
    }else{
      state.select = input.val();
      selectSearch(state);
    }
  });
  $('a#refresh-cw').on('click', function(){
    $('input[name="search"]').val('');
    $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
    $('tbody#table-body').empty();
    $('span#start').empty();
    $('span#end').empty();
    $('span#total').empty();
    $('ul#pagination-wrapper').empty();
    delete state.select;
    delete state.option;
    buildTable(state);
  })
}
// Agent database check page.
else if (document.getElementById('database_ag')) {
  $('select#from_client').on('change', checkFrom);
  $('tbody#table-body').html('<tr><td style="color: orange;" colspan="13" class="text-center">กรุณาเลือก</td></tr>')
  var state = {
    'type': "database",
    'from': "",
    'querySet': [],
    'page': 1,
    'rows': 10,
    'window': 10,
  }
  function checkFrom(){
    $('input[name=search]').val("");
    $('tbody#table-body').empty()
    $('tr#tr_header').html(`<th data-tablesaw-priority="0" class="border no" style="width: 5%;"> ลำดับ </th>`)
    const value = $('select#from_client option:selected').val()
    state.from = value;
    // Set array to show head table
    const truthColumns = value.split(":")[1].split(",")
    // After select from client
    // Set head table
    const setHead = (a, i) =>{
      if(i == a.length+2) {
        return -1
      }else if (i+1 == a.length+2){
        $('tr#tr_header').append(`
<th scope="col" data-tablesaw-priority="${i+1}" class="border"> ตัวเลือก </th>
`)
        return setHead(a, (i+1))
      }else if (i+2 == a.length+2){
        $('tr#tr_header').append(`
<th scope="col" data-tablesaw-priority="${i+1}" class="border"> วัน/เวลาที่บันทึก </th>
`)
        return setHead(a, (i+1))
      }else{
        $('tr#tr_header').append(`
<th scope="col" data-tablesaw-priority="${i+1}" class="border"> ${a[i]} </th>
`)
        return setHead(a, (i+1))
      }
    }
    // Call func
    setHead(truthColumns, 0)
    delete state['select']
    buildTable(state, 0);
  }
  $('input[name=search]').on('change keyup keydown', function(){
    searchData('database', $(this).val().trim(), $('select#from_client').find(":selected").val());
  })
  $('button:button#btn-search').on('click', function(){
    // select search here not same other agent. by if cannot select can't search.
    let input = $('input[name="search"]');
    if (input.val() === ""){
      input.css({ 'border-stype': 'soild', 'border-color': 'red' });
    }else{
      state.select = input.val()
      selectSearch(state);
    }
  })
  $('a#refresh-cw').on('click', function(){
    const checkLenOne = (t) =>{
      if (t.children()[0].querySelectorAll('td').innerHTML !== "กรุณาเลือก"){
        return true
      }else if(t.children()[0].querySelectorAll('tr')){
        return true
      }else{
        return false
      }
    }
    if( $('tbody#table-body').children().length !== 1 || checkLenOne($('tbody#table-body'))){
      $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
      delete state['select']
      checkFrom();
    }else{
      $('div#err-search').css({'overflow-y': "auto", 'display': ""});
      $('button:button#btn-err').on('click', function(){
        $('div#err-search').css('display', 'none');
      });
    }
  })
  function _delete(value) {
    const id = value.split("&&")[0]
    const no = value.split("&&")[1]
    $.post(`/${subfolder}database_ag/selectDel`,{ id: id }).done(function(result){
      $('span#db_no').empty();
      $('table#_delete_ tr').remove();
      result.column.push("วันที่/เวลาที่บันทึก")
      const display = (d, c, j) =>{
        if (j == c.length-1) {
          $('table#_delete_').append(`<tr><th style="text-align: left; width: 30%;">${c[j]}: </th><td style="text-align: left;"><span>${convert_datetime(new Date(d[j]), d[j])}</span></td></tr>`)
        }else{
          $('table#_delete_').append(`<tr><th style="text-align: left; width: 30%;">${c[j]}: </th><td style="text-align: left;"><span>${d[j]}</span></td></tr>`)
          return display(d, c, (j+1))
        }
      }
      $('table#_delete_').append(`<tr><th style="text-align: left; width: 30%;">ลำดับ: </th><td style="text-align: left;">${no}</td></tr>`)
      display(Object.values(result.data[0]).slice(1, Object.values(result.data[0]).length), result.column, 0)
      $('button#button_delete_db').attr('data-value', result.data[0].id)
    })
  }
}
// Agent log0 hash page.
else if (document.getElementById('logger_hash_ag')) {
  $('input[name="search"]').on('change keyup keydown', function(){
    searchData('log0', $(this).val().trim(), null);
  })
  var state = {
    'type': "log0",
    'querySet': [],
    'page': 1,
    'rows': 10,
    'window': 10,
  }
  const createTableContent = () =>{
      buildTable(state, 0);
  }
  createTableContent();
  $('button:button#btn-search').on('click', function(){
    /* have two method 
1). send id and get someone same id. or
2). send same search normal input and get same all this.
*/
    let input = $('input[name="search"]');
    if (input.val() === ""){
      input.css({ 'border-stype': 'soild', 'border-color': 'red' });
    }else{
      state.select = input.val()
      selectSearch(state);
    }
  })
  $('a#refresh-cw').on('click', function(){
    $('input[name="search"]').val('');
    $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
    $('tbody#table-body').empty();
    $('span#start').empty();
    $('span#end').empty();
    $('span#total').empty();
    $('ul#pagination-wrapper').empty();
    delete state.select;
    createTableContent();
  })
  function detail(id) {
    $.ajax({
      method: "POST",
      url: `/${subfolder}logger_ag/select`,
      data: { id: id },
      success: function (result) {
        $('#header').html("ข้อมูลของ: " + result.hash[0].device_name + ",<br/>" + result.hash[0].name_file)
        $('#size_logger_ag').html(result.hash[0].total_line)
        $('#recording_time_logger_ag').html(result.hash[0].date_now)
        if (result.sys[0].view_hash_log_export === "md5"){
          $('span#type_hash').text("MD5")
          $('#hash_value_logger_ag').html(result.hash[0].value_md5)
        }else if (result.sys[0].view_hash_log_export === "sha1"){
          $('span#type_hash').text("SHA-1")
          $('#hash_value_logger_ag').html(result.hash[0].value_sha1)
        }else{
          $('span#type_hash').text("SHA-256")
          $('#hash_value_logger_ag').html(result.hash[0].value)
        }
      }
    })
  }
}
// Agent manage detail page
else if(document.getElementById('a_detail')){
  let count = 0;
  $('a#show-password').on('click', function(){
    $.post(`/${subfolder}agent_manage/selectManage`,{value: "@lltr@@gentSe1ectM@n@ge", id: $(this).attr('data-value')}).done(function(result){
      let obj = result.manage[0].config_detail.split("&")
      if (count == 0){
        $('span#password').text(obj[3])
        $('a#show-password').text('ปิดการมองเห็น')
        count+=1
      }else{
        let conv_passwd = ""
        for (i in obj[3]){
          conv_passwd+="*"
        }
        $('span#password').text(conv_passwd)
        $('a#show-password').text('เปิดการมองเห็น')
        count = 0
      }
    })
  })
  function clipboard(e){
    let parent = e.parentNode.parentNode;
    let copyText = parent.getElementsByClassName('controller')[0];
    navigator.clipboard.writeText(copyText.value);
    parent.getElementsByTagName('span')[1].innerHTML = "Copied."
  }
  function outFunc(){
    document.getElementsByClassName('Tooltip').innerHTML = "Copy to clipboard"
  }
}
// Agent procedure unix page
else if(document.getElementById('a_procedure')){
  function clipboard(e){
    let parent = e.parentNode.parentNode;
    let copyText = parent.getElementsByClassName('controller')[0].value;
    const unsecuredCopyToClipboard = (text) => { 
      const textArea = document.createElement("textarea"); 
      textArea.value=text; document.body.appendChild(textArea); 
      textArea.focus();textArea.select(); 
      try{ document.execCommand('copy') }
      catch(err){ console.error('Unable to copy to clipboard',err) }
      document.body.removeChild(textArea)
    };
    const copyToClipboard = (content) => {
      if (window.isSecureContext && navigator.clipboard) {
        navigator.clipboard.writeText(content);
        parent.getElementsByTagName('span')[0].innerHTML = "Copied."
      } else {
        unsecuredCopyToClipboard(content);
      }
    };
    copyToClipboard(copyText)
  }
  function outFunc(){
    document.getElementsByClassName('Tooltip').innerHTML = "Copy to clipboard"
  }
}
// Agent procedure windows page
else if(document.getElementById('a_procedure1')){
  function clipboard(e){
    let parent = e.parentNode.parentNode;
    let copyText = parent.getElementsByClassName('controller')[0].value;
    const unsecuredCopyToClipboard = (text) => { 
      const textArea = document.createElement("textarea"); 
      textArea.value=text; document.body.appendChild(textArea); 
      textArea.focus();textArea.select(); 
      try{ document.execCommand('copy') }
      catch(err){ console.error('Unable to copy to clipboard',err) }
      document.body.removeChild(textArea)
    };
    const copyToClipboard = (content) => {
      if (window.isSecureContext && navigator.clipboard) {
        navigator.clipboard.writeText(content);
        parent.getElementsByTagName('span')[0].innerHTML = "Copied."
      } else {
        unsecuredCopyToClipboard(content);
      }
    };
    copyToClipboard(copyText)
  }
  function outFunc(){
    document.getElementsByClassName('Tooltip').innerHTML = "Copy to clipboard"
  }
}
// Agent manage new page.
else if (document.getElementById('a_new')) { 
  // ======================= Add =============================
  $.post(`/${subfolder}agent_manage/name`, {'value': "@lltr@@gentN@me"}).done(function(r) {
    $('input[name="agm_name"]#name-agent').on('keypress keyup keydown change focus paste', function(e){
      if(e.which == 13 || e.which == 44){
        return false;
      }
      let val = $(this).val().trim()
      let selected = r.filter(function(i){ if (i.agm_name === val){ return i }})
      if(selected.length > 0 && val.length > 0){
        $('div#checkName').html('<span style="font-size: 15px; color: red;">มีผู้ใช้งานแล้ว</span>')
      }else if(val.length < 1){
        $('div#checkName').empty()
      }else{
        $('div#checkName').html('<span style="font-size: 15px; color: green;">สามารถใช้งานได้</span>')
      }
    })
  })
  $('select#select-agent').on('change',function(){
    $('select#select-agent option').each(function(){
      if($(this).is(':selected')){
        $.post(`/${subfolder}agent_manage/selectStore`,{value: "@lltr@@gentSe1ectStore", id: $(this).val()}).done(function(result){
          if (result[0].hide == 0 && result[0].status == 1){
            $('span#name').text("("+result[0].name+")")
            $('table#head-agent').css('display', 'block');
            $('div#content-agent').html(`
<textarea class="form-control text-center" rows="7" style="resize: none;" readonly>

รหัสของ Aegnt: ${result[0].code}
ประเภทของ Agent: ${result[0].type}
จำนวนที่สามารถใช้งานของ Agent: ${result[0]._limit_}
คำอธิบายของ Agent: 
${result[0].description}
</textarea>
`)
          }
          if (result[0].code == "AG1" ){ // Agent new log0
            detailConfig(result[0].code, result[0].name, null)
            $('button:button#add-path').on('click', function(){
              if($('input[name="path"]').val()){
                $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                $('input[name="path"]').val("");
              }
              if ($("input[name='total_path']").val()){
                $('div#show-tags-path').empty()
                let total = $('input[name="total_path"]').val().split(",")
                total = total.filter(i => i)
                for ( let i in total ){
                  $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}">${total[i]}</button>`)
                }
              }else{
                $('div#show-tags-path').html('<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>')
              }
              $('button#del-tags').on('click', function(){
                let value = $(this).attr('data-value')
                let total = $('input[name="total_path"]').val().split(",")
                total.pop()
                total = total.filter(i => i !== value && i !== "")
                $('input[name="total_path"]').val(total+",")
                if (total.length > 0){
                  for ( let i = 0;i < $('div#show-tags-path').children().length;i++){
                    if ($('div#show-tags-path').children()[i].getAttribute('data-value') === value){
                      $('div#show-tags-path').children()[i].remove()
                    }
                  }
                }else{
                  $('div#show-tags-path').html('<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>')
                }
              })
            })
          }
          else if (result[0].code == "AG2"){ // Agent new file/dir
            detailConfig(result[0].code, result[0].name, null)
            $('input:radio').on('click', function(){
              if($(this).val() == -1){
                $('div#subject-content').html(`
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> เลือกชนิดของไฟล์ </th>
<td style="text-align: left;">
<select name="extension" class="form-control" required>
<option value disabled selected> กรุณาเลือก </option>
<option value=".log"> log </option>
<option value=".evtx"> log (Windows) </option>
<option value=".csv"> csv </option>
<option value=".xls"> xls </option>
<option value=".xlsx"> xlsx </option>
</select>
<div class="invalid-feedback">
กรุณาเลือกชนิดไฟล์!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> ระบุที่อยู่ไฟล์พร้อมชื่อไฟล์ </th>
<td style="text-align: left;">
<div class="form-group">
<input type="text" name="total_path" placeholder="กรุณาป้อนที่อยู่พร้อมชื่อไฟล์..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อนที่อยู่พร้อมชื่อไฟล์!!!
</div>
<br/>
<span> <u>ตัวอย่างเช่น</u>: /home/user/work1/file หรือ C:\\Users\\Admin\\Log\\file </span>
</div>
</td>
</table>
`)
                $('div#show-tags-path').html('<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>')
              }else if($(this).val() == 0){
                $('div#subject-content').html(`
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> เลือกชนิดของไฟล์ </th>
<td style="text-align: left;">
<select name="extension" class="form-control" required>
<option value disabled selected> กรุณาเลือก </option>
<option value=".log"> log </option>
<option value=".evtx"> log (Windows) </option>
<option value=".csv"> csv </option>
<option value=".xls"> xls </option>
<option value=".xlsx"> xlsx </option>
</select>
<div class="invalid-feedback">
กรุณาเลือกชนิดของไฟล์
</div>
</td>
</table>
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> ระบุที่อยู่ไฟล์ </th>
<td style="text-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่และชื่อไฟล์..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: /home/user/work1/  หรือ C:\\Users\\Admin\\Log\\
</div>
<input type="text" name="total_path" readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
                $('div#show-tags-path').html('<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>')
                $('button:button#add-path').on('click', function(){
                  if($('input[name="path"]').val()){
                    $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                    $('input[name="path"]').val("");
                  }else{
                    $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
                    $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                      $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
                    })
                  }
                  if ($("input[name='total_path']").val()){
                    $('div#show-tags-path').empty()
                    let total = $('input[name="total_path"]').val().split(",")
                    total = total.filter(i => i)
                    for ( let i in total ){
                      $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}"> ${total[i]} </button>`)
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                  $('button#del-tags').on('click', function(){
                    let value = $(this).attr('data-value')
                    let total = $('input[name="total_path"]').val().split(",")
                    total.pop()
                    total = total.filter(i => i != value && i !== "")
                    $('input[name="total_path"]').val(total+",")
                    if (total.length > 0){
                      for ( let i = 0;i < $('div#show-tags-path').children().length;i++){
                        if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                          $('div#show-tags-path').children()[i].remove()
                        }
                      }
                    }else{
                      $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                    }
                  })
                })
              }else if($(this).val() == 1){ 
                $('div#subject-content').html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${result[0].name} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
ระบุที่อยู่ไฟล์จราจร
</th>
<td style="tet-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: /home/user/work1/  หรือ C:\\Users\\Admin\\Log\\ 
</div>
<input type="text" name="total_path" readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
                $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                $('button:button#add-path').on('click', function(){
                  if($('input[name="path"]').val()){
                    $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                    $('input[name="path"]').val("");
                  }else{
                    $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
                    $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                      $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
                    })
                  }
                  if ($("input[name='total_path']").val()){
                    $('div#show-tags-path').empty()
                    let total = $('input[name="total_path"]').val().split(",")
                    total = total.filter(i => i)
                    for ( let i in total ){
                      $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}"> ${total[i]} </button>`)
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                  $('button#del-tags').on('click', function(){
                    let value = $(this).attr('data-value')
                    let total = $('input[name="total_path"]').val().split(",")
                    total.pop()
                    total = total.filter(i => i !== value && i !== "")
                    $('input[name="total_path"]').val(total+",")
                    if (total.length > 0){
                      for ( let i = 0;i < $('div#show-tags-path').children().length;i++){
                        if ($('div#show-tags-path').children()[i].getAttribute('data-value') === value){
                          $('div#show-tags-path').children()[i].remove()
                        }
                      }
                    }else{
                      $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                    }
                  })
                })
              }
            })
          }
          else if (result[0].code == "AG3"){ // Agent new database
            detailConfig(result[0].code, result[0].name, null);
            $('button:button#testDB').on('click', function(){
              let type = $('input[name="service_database"]:checked').val();
              let obj = {'host': $('input[name="host"]').val(), 'username': $('input[name="username"]').val(), 'password': $('input[name="password"]').val(), 'database': $('input[name="database"]').val()}
              if(inputCheckNull(obj) == 1){
                $("div#alert-loading").css('display', 'flex')
                $.post(`/${subfolder}agent_manage/connect/`,{value:"@lltr@@gentM@n@geTestConnect",host: obj.host,user: obj.username,pass: obj.password, db: obj.database, type: type}, "json").done(function(rs){
                  $("div#alert-loading").css('display', 'none')
                  let fm = findMatchObject(rs, "err")
                  if(fm === true && (parseInt(type) === 1 || parseInt(type) === 0)){
                    $('h2#alert-success.swal2-title').text("ไม่สามารถเชื่อมต่อได้...")
                    $('div#alert-content-success.swal2-content').html(`
CODE: ${rs.code}<br/>
ERROR: ${rs.err}
`)
                    $('div#alert-warning-icon.swal2-icon.swal2-warning.swal2-animate-warning-icon').css('display', "flex")
                    $('div#alert-success-icon.swal2-icon.swal2-success.swal2-animate-success-icon').css('display', "none")
                    $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'flex');
                    $('button#alert-success.swal2-confirm.swal2-styled').on('click', function(){
                      $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'none');
                    })
                    $('input[name="host"]').val("")
                    $('input[name="username"]').val("")
                    $('input[name="password"]').val("")
                    $('input[name="database"]').val("")
                  }else if(fm === false && parseInt(type) === 0 && Object.keys(rs).length === 1){
                    $('h2#alert-success.swal2-title').text("ไม่สามารถเชื่อมต่อได้...")
                    $('div#alert-content-success.swal2-content').html(`
ErrorCode: ${rs.message.split(":")[0]}<br/>
Description: ${rs.message.split(":").splice(1).join("")}
`)
                    $('div#alert-warning-icon.swal2-icon.swal2-warning.swal2-animate-warning-icon').css('display', "flex")
                    $('div#alert-success-icon.swal2-icon.swal2-success.swal2-animate-success-icon').css('display', "none")
                    $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'flex');
                    $('button#alert-success.swal2-confirm.swal2-styled').on('click', function(){
                      $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'none');
                    })
                    $('input[name="host"]').val("")
                    $('input[name="username"]').val("")
                    $('input[name="password"]').val("")
                    $('input[name="database"]').val("")
                  }else if(fm === false && (parseInt(type) === 0 || parseInt(type) === 1)){
                    $('h2#alert-success.swal2-title').text(rs.message)
                    $('div#alert-content-success.swal2-content').empty()
                    $('div#alert-warning-icon.swal2-icon.swal2-warning.swal2-animate-warning-icon').css('display', "none")
                    $('div#alert-success-icon.swal2-icon.swal2-success.swal2-animate-success-icon').css('display', "flex")
                    $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'flex');
                    $('button#alert-success.swal2-confirm.swal2-styled').on('click', function(){
                      $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'none');
                    })
                    $('table#show-tables').css('display', 'block')
                    if(Object.keys(rs.result).length > 0){
                      $('div#subject-table').empty()
                      let tables = rs.result;
                      for (let i in tables){
                        appendTable(tables[i].name, tables[i].columns)
                      }
                      let bkup = ""
                      $('input:radio[name="selectTable"]').on('click', function(){
                        if(bkup === ""){
                          displayColumns(document.querySelector(`span[data-value="${$(this).val()}`));
                          for (const i in document.querySelector(`div#column-${$(this).val()}`).children){
                            (!isNaN(Number(i))) ? document.querySelector(`div#column-${$(this).val()}`).children[i].querySelector(`input[name="${$(this).val()}_selectCol_${i}"]`).setAttribute('checked', true) : -1
                          }
                          bkup = $(this).val();
                        }else if(bkup !== $(this).val()){
                          for (const i in document.querySelector(`div#column-${bkup}`).children){
                            (!isNaN(Number(i))) ? document.querySelector(`div#column-${bkup}`).children[i].querySelector(`input[name="${bkup}_selectCol_${i}"]`).removeAttribute('checked') : -1
                          }
                          displayColumns(document.querySelector(`span[data-value="${bkup}`));
                          displayColumns(document.querySelector(`span[data-value="${$(this).val()}`));
                          for (const i in document.querySelector(`div#column-${$(this).val()}`).children){
                            (!isNaN(Number(i))) ? document.querySelector(`div#column-${$(this).val()}`).children[i].querySelector(`input[name="${$(this).val()}_selectCol_${i}"]`).setAttribute('checked', true) : -1
                          }
                          bkup = $(this).val();
                        }
                      })
                    }else{
                      $('div#subject-table').html(`<span style="color: red;">ไม่พบข้อมูลใน database.</span>`)
                    }
                  }else{
                    console.log(rs)
                  }
                })
              }
            })
          }
          else if (result[0].code == "AG4"){ // Agent new sniffer
            detailConfig(result[0].code, result[0].name, null);
            if(document.getElementById('mode').checked && document.getElementById('mode').value === "0"){
              $('div#subject-mode').html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> ระบุที่อยู่ของไฟล์เพื่อจัดเก็บ </th>
<td style="text-align: left;">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" /><br/>
<u>ตัวอย่าง</u>: ./sniffer/ หรือ /home/user/sniffer/
</td>
</table>
`);
            }else{
              $('div#subject-mode').empty();
            }
          }
          else{
            $('div#detail-config').empty()
          }
        })
      }
    })
  })
  document.getElementById('confirmAdd').addEventListener("click", function(e){
    if(typeof document.querySelector('div[id="checkName"] > span').textContent !== 'undefined'){
      if(document.querySelector('div[id="checkName"] > span').textContent !== "สามารถใช้งานได้"){
        e.preventDefault()
      }else{
        document.getElementById("actionAdd").submit();
      }
    }
  })
}
// Agent manage edit page.
else if (document.getElementById('a_edit')) {
  // ======================= Edit =============================
  $('input[name="agm_name"]#name-agent').on('keypress keyup keydown', function(e){
    if(e.which == 13 || e.which == 44){
      return false;
    }
  })
  $.post(`/${subfolder}agent_manage/selectManage`,{value: "@lltr@@gentSe1ectM@n@ge", id: $('div#a_edit').attr('data-value')}).done(function(res){
    if($('span#select-agent').attr('data-value')){
      $.post(`/${subfolder}agent_manage/selectStore`,{value: "@lltr@@gentSe1ectStore", id: $('span#select-agent').attr('data-value')}).done(function(result){
        if (result[0].hide == 0 && result[0].status == 1){ $('span#name').text("("+result[0].name+")")
          $('table#head-agent').css('display', 'block');
          $('div#content-agent').html(`
<textarea class="form-control text-center" rows="7" style="resize: none;" readonly>

รหัสของ Aegnt: ${result[0].code}
ประเภทของ Agent: ${result[0].type}
จำนวนที่สามารถใช้งานของ Agent: ${result[0]._limit_}
คำอธิบายของ Agent: 
${result[0].description}
</textarea>
`)
        }
        let old_value = res.manage[0].config_detail
        let regex = [".log", ".evtx", ".csv", ".xls", ".xlsx"]
        if (result[0].code == "AG1" ){ // Agent manage edit log0
          let keys = {'checked': ["", "", ""], 'hash_name': "", 'old_value': old_value};
          if(res.manage[0].hash_type == -1){
            keys.checked[0] = "checked"
            keys.hash_name = "MD5"
          }else if(res.manage[0].hash_type == 0){
            keys.checked[1] = "checked"
            keys.hash_name = "SHA-1"
          }else if(res.manage[0].hash_type == 1){
            keys.checked[2] = "checked"
            keys.hash_name = "SHA-256"
          }
          detailConfig(result[0].code, result[0].name, keys)
          if ($("input[name='total_path']").val()){
            $('div#show-tags-path').empty()
            let total = $('input[name="total_path"]').val().split(",")
            total = total.filter(i => i)
            for ( i in total ){
              $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}">${total[i]} </button>`)
            }
          }else{
            $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
          }
          $('button:button#add-path').on('click', function(){
            if($('input[name="path"]').val()){
              $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
              $('input[name="path"]').val("");
            }else{
              $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
              $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
              })
            }
            if ($("input[name='total_path']").val()){
              $('div#show-tags-path').empty()
              let total = $('input[name="total_path"]').val().split(",")
              total = total.filter(i => i)
              for ( i in total ){
                $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}"> ${total[i]} </button>`)
              }
            }else{
              $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
            }
            $('button#del-tags').on('click', function(){
              let value = $(this).attr('data-value')
              let total = $('input[name="total_path"]').val().split(",")
              total.pop()
              total = total.filter(i => i != value && i !== "")
              $('input[name="total_path"]').val(total+",")
              if (total.length > 0){
                for ( i=0;i < $('div#show-tags-path').children().length;i++){
                  if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                    $('div#show-tags-path').children()[i].remove()
                  }
                }
              }else{
                $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
              }
            })
          })
          $('button#del-tags').on('click', function(){
            let value = $(this).attr('data-value')
            let total = $('input[name="total_path"]').val().split(",")
            total.pop()
            total = total.filter(i => i != value && i !== "")
            $('input[name="total_path"]').val(total+",")
            if (total.length > 0){
              for ( i=0;i < $('div#show-tags-path').children().length;i++){
                if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                  $('div#show-tags-path').children()[i].remove()
                }
              }
            }else{
              $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
            }
          })
        }else if (result[0].code == "AG2"){ // Agent manage edit file/dir
          old_value =  old_value.split(",")
          let keys = {'check': ["", "", ""], old_value: old_value};
          if(old_value.length == 1 && regex.indexOf(old_value[0].substring(old_value[0].length - 4)) !== -1){
            keys.check[0] = "checked"
          }else{
            if(findLogic(old_value).indexOf(true) !== -1){
              keys.check[1] = "checked"
            }else{
              keys.check[2] = "checked"
            }
          }
          detailConfig(result[0].code, result[0].name, keys)
          if($('input:radio:checked').val()){
            if($('input:radio:checked').val() == -1){
              let subject = old_value[0].split('.')
              if(subject.length > 2){
                subject.shift()
                subject[0] = "."+subject[0]
                subject[1] = "."+subject[1]
              }else{
                subject[1] = "."+subject[1]
              }
              let select = ["", "", "", ""]
              if (regex.indexOf(subject[1]) == 0){
                select[0] = "selected"
              }else if(regex.indexOf(subject[1]) == 1){
                select[1] = "selected"
              }else if(regex.indexOf(subject[1]) == 2){
                select[2] = "selected"
              }else if(regex.indexOf(subject[1]) == 3){
                select[3] = "selected"
              }else if(regex.indexOf(subject[1]) == 4){
                select[4] = "selected"
              }
              $('div#subject-content').html(`
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> เลือกชนิดของไฟล์ </th>
<td style="text-align: left;">
<select name="extension" class="form-control" required>
<option value disabled selected> กรุณาเลือก </option>
<option value=".log" ${select[0]}> log </option>
<option value=".evtx" ${select[1]}> log (Windows) </option>
<option value=".csv" ${select[2]}> csv </option>
<option value=".xls" ${select[3]}> xls </option>
<option value=".xlsx" ${select[4]}> xlsx </option>
</select>
<div class="invalid-feedback">
กรุณาเลือกชนิดของไฟล์!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> ระบุที่อยู่ไฟล์พร้อมชื่อไฟล์ </th>
<td style="text-align: left;">
<div class="form-group">
<input type="text" name="total_path" value="${subject[0]}" placeholder="กรุณาป้อนที่อยู่และชื่อไฟล์..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อนที่อยู่พร้อมชื่อไฟล์!!!
</div>
<br/>
<span> <u>ตัวอย่างเช่น</u>: ./work1/file หรือ /home/user/work1/file หรือ C:\\Users\\Admin\\Log\\file </span>
</div>
</td>
</table>
`)
            }else if($('input:radio:checked').val() == 0){
              let subject = []
              old_value.forEach(function(i, d){
                if(parseInt(d)+1 == old_value.length){
                  subject.push(i.split("*"))
                }else{
                  subject.push(i.split("*")[0])
                }
              })
              subject = subject.concat(subject[subject.length - 1])
              subject = subject.filter(i => typeof i === "string")
              let type = subject.pop()
              let select = ["", "", "", ""]
              if(regex.indexOf(type) == 0){
                select[0] = "selected"
              }else if(regex.indexOf(type) == 1){
                select[1] = "selected"
              }else if(regex.indexOf(type) == 2){
                select[2] = "selected"
              }else if(regex.indexOf(type) == 3){
                select[3] = "selected"
              }else if(regex.indexOf(type) == 4){
                select[4] = "selected"
              }
              $('div#subject-content').html(`
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> เลือกชนิดของไฟล์ </th>
<td style="text-align: left;">
<select name="extension" class="form-control" required>
<option value disabled selected> กรุณาเลือก </option>
<option value=".log" ${select[0]}> log </option>
<option value=".evtx" ${select[1]}> log (Windows) </option>
<option value=".csv" ${select[2]}> csv </option>
<option value=".xls" ${select[3]}> xls </option>
<option value=".xlsx" ${select[4]}> xlsx </option>
</select>
<div class="invalid-feedback">
กรุณาเลือกชนิดของไฟล์!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> ระบุที่อยู่ไฟล์ </th>
<td style="text-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่และชื่อไฟล์..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: ./work1/ หรือ /home/user/work1/ หรือ C:\\Users\\Admin\\Log\\
</div>
<input type="text" name="total_path" value="${subject}," readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
              if($('input[name="total_path"]').val()){
                $('div#show-tags-path').empty()
                let total = $('input[name="total_path"]').val().split(",")
                total = total.filter(i => i)
                for ( i in total ){
                  $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}"> ${total[i]} </button>`)
                }
                $('button#del-tags').on('click', function(){
                  let value = $(this).attr('data-value')
                  let total = $('input[name="total_path"]').val().split(",")
                  total.pop()
                  total = total.filter(i => i != value && i !== "")
                  $('input[name="total_path"]').val(total+",")
                  if (total.length > 0){
                    for ( i=0;i < $('div#show-tags-path').children().length;i++){
                      if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                        $('div#show-tags-path').children()[i].remove()
                      }
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                })
                $('button:button#add-path').on('click', function(){
                  if($('input[name="path"]').val()){
                    $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                    $('input[name="path"]').val("");
                  }else{
                    $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
                    $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                      $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
                    })
                  }
                  if ($("input[name='total_path']").val()){
                    $('div#show-tags-path').empty()
                    let total = $('input[name="total_path"]').val().split(",")
                    total = total.filter(i => i)
                    for ( i in total ){
                      $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}"> ${total[i]} </button>`)
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                  $('button#del-tags').on('click', function(){
                    let value = $(this).attr('data-value')
                    let total = $('input[name="total_path"]').val().split(",")
                    total.pop()
                    total = total.filter(i => i != value && i !== "")
                    $('input[name="total_path"]').val(total+",")
                    if (total.length > 0){
                      for ( i=0;i < $('div#show-tags-path').children().length;i++){
                        if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                          $('div#show-tags-path').children()[i].remove()
                        }
                      }
                    }else{
                      $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                    }
                  })
                })
              }
            }else if($('input:radio:checked').val() == 1){
              $('div#subject-content').html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${result[0].name} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
ระบุที่อยู่ไฟล์จราจร
</th>
<td style="tet-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: ./work1/ หรือ /home/user/work1/ หรือ C:\\Users\\Admin\\Log\\
</div>
<input type="text" name="total_path" value="${old_value}," readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
              if($('input[name="total_path"]').val()){
                $('div#show-tags-path').empty()
                let total = $('input[name="total_path"]').val().split(",")
                total = total.filter(i => i)
                for ( i in total ){
                  $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}">${total[i]} </button>`)
                }
                $('button#del-tags').on('click', function(){
                  let value = $(this).attr('data-value')
                  let total = $('input[name="total_path"]').val().split(",")
                  total.pop()
                  total = total.filter(i => i != value && i !== "")
                  $('input[name="total_path"]').val(total+",")
                  if (total.length > 0){
                    for ( i=0;i < $('div#show-tags-path').children().length;i++){
                      if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                        $('div#show-tags-path').children()[i].remove()
                      }
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                })
                $('button:button#add-path').on('click', function(){
                  if($('input[name="path"]').val()){
                    $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                    $('input[name="path"]').val("");
                  }else{
                    $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
                    $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                      $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
                    })
                  }
                  if ($("input[name='total_path']").val()){
                    $('div#show-tags-path').empty()
                    let total = $('input[name="total_path"]').val().split(",")
                    total = total.filter(i => i)
                    for ( i in total ){
                      $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}">${total[i]} </button>`)
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                  $('button#del-tags').on('click', function(){
                    let value = $(this).attr('data-value')
                    let total = $('input[name="total_path"]').val().split(",")
                    total.pop()
                    total = total.filter(i => i != value && i !== "")
                    $('input[name="total_path"]').val(total+",")
                    if (total.length > 0){
                      for ( i=0;i < $('div#show-tags-path').children().length;i++){
                        if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                          $('div#show-tags-path').children()[i].remove()
                        }
                      }
                    }else{
                      $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                    }
                  })
                })
              }
            }
          }
          $('input:radio').on('click', function(){
            if($(this).val() == -1){
              $('div#subject-content').html(`
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> เลือกชนิดของไฟล์ </th>
<td style="text-align: left;">
<select name="extension" class="form-control" required>
<option value disabled selected> กรุณาเลือก </option>
<option value=".log"> log </option>
<option value=".csv"> csv </option>
<option value=".xls"> xls </option>
<option value=".xlsx"> xlsx </option>
</select>
<div class="invalid-feedback">
กรุณาเลือกชนิดของไฟล์!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> ระบุที่อยู่ไฟล์พร้อมชื่อไฟล์ </th>
<td style="text-align: left;">
<div class="form-group">
<input type="text" name="total_path" placeholder="กรุณาป้อนที่อยู่และชื่อไฟล์..." class="form-control" required/>
<div class="invalid-feedback">
กรุณาป้อนที่อยู่พร้อมชื่อไฟล์!!!
</div>
<br/>
<span> <u>ตัวอย่างเช่น</u>: ./work1/file หรือ /home/user/work1/file </span>
</div>
</td>
</table>
`)
              $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
            }else if($(this).val() == 0){
              $('div#subject-content').html(`
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> เลือกชนิดของไฟล์ </th>
<td style="text-align: left;">
<select name="extension" class="form-control" required>
<option value disabled selected> กรุณาเลือก </option>
<option value=".log"> log </option>
<option value=".csv"> csv </option>
<option value=".xls"> xls </option>
<option value=".xlsx"> xlsx </option>
</select>
<div class="invalid-feedback">
กรุณาเลือกชนิดของไฟล์!!!
</div>
</td>
</table>
<table class="table table-boardless">
<th style="text-align: left; font-weight: bold; width: 12%;"> ระบุที่อยู่ไฟล์ </th>
<td style="text-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่และชื่อไฟล์..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: ./work1/ หรือ /home/user/work1/
</div>
<input type="text" name="total_path" readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
              $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
              $('button:button#add-path').on('click', function(){
                if($('input[name="path"]').val()){
                  $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                  $('input[name="path"]').val("");
                }else{
                  $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
                  $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                    $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
                  })
                }
                if ($("input[name='total_path']").val()){
                  $('div#show-tags-path').empty()
                  let total = $('input[name="total_path"]').val().split(",")
                  total = total.filter(i => i)
                  for ( i in total ){
                    $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}">${total[i]} </button>`)
                  }
                }else{
                  $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                }
                $('button#del-tags').on('click', function(){
                  let value = $(this).attr('data-value')
                  let total = $('input[name="total_path"]').val().split(",")
                  total.pop()
                  total = total.filter(i => i != value && i !== "")
                  $('input[name="total_path"]').val(total+",")
                  if (total.length > 0){
                    for ( i=0;i < $('div#show-tags-path').children().length;i++){
                      if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                        $('div#show-tags-path').children()[i].remove()
                      }
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                })
              })
            }else if($(this).val() == 1){
              $('div#subject-content').html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold;"> รายการตั้งค่า ${result[0].name} เพื่อนำไปใช้งาน </th>
</table>
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> 
ระบุที่อยู่ไฟล์จราจร
</th>
<td style="tet-align: left;">
<div class="input-group">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" />
<button id="add-path" type="button" class="btn btn-outline-success"><i class="mdi mdi-plus-circle-outline" style="font-size: 15px;"></i></button>
</div>
<div style="margin-top:1%; text-align: left;">
<u>ตัวอย่างเช่น</u>: ./work1/ หรือ /home/user/work1/
</div>
<input type="text" name="total_path" readonly hidden/>
<div id="show-tags-path" style="text-align: left; margin-top:1%;">
</div>
</td>
</table>
`)
              $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
              $('button:button#add-path').on('click', function(){
                if($('input[name="path"]').val()){
                  $('input[name="total_path"]').val($('input[name="total_path"]').val()+$('input[name="path"]').val()+",")
                  $('input[name="path"]').val("");
                }else{
                  $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','flex')
                  $('button:button#alert-table.swal2-cancel.swal2-styled').on('click', function(){
                    $('div#alert-table.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display','none')
                  })
                }
                if ($("input[name='total_path']").val()){
                  $('div#show-tags-path').empty()
                  let total = $('input[name="total_path"]').val().split(",")
                  total = total.filter(i => i)
                  for ( i in total ){
                    $('div#show-tags-path').append(`<button type="button" id="del-tags" class="btn btn-light-success text-success px-4 rounded-pill font-weight-medium" data-value="${total[i]}">${total[i]} </button>
`)
                  }
                }else{
                  $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                }
                $('button#del-tags').on('click', function(){
                  let value = $(this).attr('data-value')
                  let total = $('input[name="total_path"]').val().split(",")
                  total.pop()
                  total = total.filter(i => i != value && i !== "")
                  $('input[name="total_path"]').val(total+",")
                  if (total.length > 0){
                    for ( i=0;i < $('div#show-tags-path').children().length;i++){
                      if ($('div#show-tags-path').children()[i].getAttribute('data-value') == value){
                        $('div#show-tags-path').children()[i].remove()
                      }
                    }
                  }else{
                    $('div#show-tags-path').html(`<span style="color: red;">ไม่มีข้อมูล กรุณาเพิ่มข้อมูล</span>`)
                  }
                })
              })
            }
          })
        }else if (result[0].code == "AG3"){ // Agent manage edit database
          old_value = old_value.split("&")
          let keys = {'check': ["", ""], 'old_value': old_value, "table": "", "col": []};
          if(old_value[0] == 0){
            keys.check[0] = "checked"
          }else if(old_value[0] == 1){
            keys.check[1] = "checked"
          }
          for (let i = 5;i < old_value.length; i++){ // Today can't use many table 31/01/2023
            if(parseInt(i)+1 == old_value.length){
              keys.table += old_value[i].split(":")[0]
              keys.col.push(old_value[i].split(":")[1].split(","))
            }else{
              keys.table += old_value[i].split(":")[0]+","
              keys.col.push(old_value[i].split(":")[1].split(","))
            }
          }
          let report = ""
          if (result[0].report_db == 1){
            report = "<span style='font-size: 20px; color: green; vertical-align: middle;'> <i class='mdi mdi-check'></i> </span>"
          }else{
            report = "<span style='font-size: 20px; color: red; vertical-align: middle;'> <i class='mdi mdi-close'></i> </span>"
          }
          detailConfig(result[0].code, result[0].name, keys);
          $('button:button#testDB').on('click', function(){
            let type = $('input[name="service_database"]:checked').val();
            let obj = {'host': $('input[name="host"]').val(), 'username': $('input[name="username"]').val(), 'password': $('input[name="password"]').val(), 'database': $('input[name="database"]').val()}
            if(inputCheckNull(obj) == 1){
              $("div#alert-loading").css('display', 'flex')
              $.post(`/${subfolder}agent_manage/connect/`,{value:"@lltr@@gentM@n@geTestConnect",host: obj.host,user: obj.username,pass: obj.password, db: obj.database, type: type}, "json").done(function(rs){
                $("div#alert-loading").css('display', 'none')
                let fm = findMatchObject(rs, "err")
                if(fm === true && parseInt(type) === 1){
                  $('h2#alert-success.swal2-title').text("ไม่สามารถเชื่อมต่อได้...")
                  $('div#alert-content-success.swal2-content').html(`
CODE: ${rs.code}<br/>
ERRNO: ${rs.err}
`)
                  $('div#alert-warning-icon.swal2-icon.swal2-warning.swal2-animate-warning-icon').css('display', "flex")
                  $('div#alert-success-icon.swal2-icon.swal2-success.swal2-animate-success-icon').css('display', "none")
                  $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'flex');
                  $('button#alert-success.swal2-confirm.swal2-styled').on('click', function(){
                    $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'none');
                  })
                  $('input[name="host"]').val("")
                  $('input[name="username"]').val("")
                  $('input[name="password"]').val("")
                  $('input[name="database"]').val("")
                }else if(fm === false && parseInt(type) === 0 && Object.keys(rs).length === 1){
                  $('h2#alert-success.swal2-title').text("ไม่สามารถเชื่อมต่อได้...")
                  $('div#alert-content-success.swal2-content').html(`
ErrorCode: ${rs.message.split(":")[0]}<br/>
Description: ${rs.message.split(":").splice(1).join("")}
`)
                  $('div#alert-warning-icon.swal2-icon.swal2-warning.swal2-animate-warning-icon').css('display', "flex")
                  $('div#alert-success-icon.swal2-icon.swal2-success.swal2-animate-success-icon').css('display', "none")
                  $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'flex');
                  $('button#alert-success.swal2-confirm.swal2-styled').on('click', function(){
                    $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'none');
                  })
                  $('input[name="host"]').val("")
                  $('input[name="username"]').val("")
                  $('input[name="password"]').val("")
                  $('input[name="database"]').val("")
                }else if(fm === false && (parseInt(type) === 0 || parseInt(type) === 1)){
                  $('h2#alert-success.swal2-title').text(rs.message)
                  $('div#alert-content-success.swal2-content').empty()
                  $('div#alert-warning-icon.swal2-icon.swal2-warning.swal2-animate-warning-icon').css('display', "none")
                  $('div#alert-success-icon.swal2-icon.swal2-success.swal2-animate-success-icon').css('display', "flex")
                  $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'flex');
                  $('button#alert-success.swal2-confirm.swal2-styled').on('click', function(){
                    $('div#alert-success.swal2-container.swal2-center.swal2-fade.swal2-shown').css('display', 'none');
                  })
                  $('table#show-tables').css('display', 'block')
                  if(Object.keys(rs.result).length > 1){
                    $('div#subject-table').empty()
                    let tables = rs.result;
                    for (let i in tables){
                      appendTable(tables[i].name, tables[i].columns)
                    }
                    let bkup = ""
                    $('input:radio[name="selectTable"]').on('click', function(){
                      if(bkup === ""){
                        displayColumns(document.querySelector(`span[data-value="${$(this).val()}`));
                        for (const i in document.querySelector(`div#column-${$(this).val()}`).children){
                          (!isNaN(Number(i))) ? document.querySelector(`div#column-${$(this).val()}`).children[i].querySelector(`input[name="${$(this).val()}_selectCol_${i}"]`).setAttribute('checked', true) : -1
                        }
                        bkup = $(this).val();
                      }else if(bkup !== $(this).val()){
                        for (const i in document.querySelector(`div#column-${bkup}`).children){
                          (!isNaN(Number(i))) ? document.querySelector(`div#column-${bkup}`).children[i].querySelector(`input[name="${bkup}_selectCol_${i}"]`).removeAttribute('checked') : -1
                        }
                        displayColumns(document.querySelector(`span[data-value="${bkup}`));
                        displayColumns(document.querySelector(`span[data-value="${$(this).val()}`));
                        for (const i in document.querySelector(`div#column-${$(this).val()}`).children){
                          (!isNaN(Number(i))) ? document.querySelector(`div#column-${$(this).val()}`).children[i].querySelector(`input[name="${$(this).val()}_selectCol_${i}"]`).setAttribute('checked', true) : -1
                        }
                        bkup = $(this).val();
                      }
                    })
                  }else{
                    $('div#subject-table').html(`<span style="color: red;">ไม่พบข้อมูลใน database.</span>`)
                  }
                }
              })
            }
          })
        }else if (result[0].code == "AG4"){ // Agent manage edit sniffer
          old_value = old_value.split(",")
          let keys = {'check': ["", ""], 'old_value': old_value};
          if (old_value[0] == 0){
            keys.check[0] = "checked"
          }else if (old_value[0] == 1){
            keys.check[1] = "checked"
          }
          detailConfig(result[0].code, result[0].name, keys);
          if(document.getElementById('mode').checked && document.getElementById('mode').value === "0"){
            $('div#subject-mode').html(`
<table class="table table-boardless">
<th width="12%" style="text-align: left; font-weight: bold; vertical-align: top;"> ระบุที่อยู่ของไฟล์เพื่อจัดเก็บ </th>
<td style="text-align: left;">
<input type="text" name="path" placeholder="กรุณาป้อนที่อยู่..." class="form-control" /><br/>
<u>ตัวอย่าง</u>: ./sniffer/ หรือ /home/user/sniffer/
</td>
</table>
`);
          }else{
            $('div#subject-mode').empty();
          }
        }
        else{
          $('div#detail-config').empty()
        }
      })
    }
  })
}
// Agent sniffer page.
else if(document.getElementById('a-sniffer')){
  if ($('#table-body').children().length == 0){
    $('#table-body').append(`<tr><td class="text-center text-warning" colspan="5">กรุณาเลือกอุปกรณ์</td></tr>`);
  }   
  let state = {
    'type': "sniffer",
    'from': ``,
    'querySet': [],
    'page': 1,
    'rows': 10,
    'window': 10,
  }
  const checkFrom = (e) => {
    if (e === "Default"){
      $('select#path').on('change', function(){
        $('input[name="search"]').val('');
        $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
        $('#table-body').empty();
        state.from = `${fullpath}${$(this).val()}/`
        buildTable(state, 0)
      })
    }else{
      $('input[name="search"]').val('');
      $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
      $('#table-body').empty();
      state.from = `${fullpath}${$('select#path option:selected').val()}/`
      // console.log(state)
      buildTable(state, 0)
    }
  }
  checkFrom("Default");
  $('input[name="search"]').on('change keyup keydown', function(){
    searchData('sniffer', $(this).val().trim(), `${fullpath}${$('select#path').find(":selected").val()}`);
  })
  $('button:button#btn-search').on('click', function(){
    // select search here not same other agent. by if cannot select can't search.
    let input = $('input[name="search"]');
    if (input.val() === ""){
      input.css({ 'border-stype': 'soild', 'border-color': 'red' });
    }else{
      state.select = input.val();
      state.option = `${fullpath}${$('select#path').find(":selected").val()}/`
      selectSearch(state);
    }
  })
  $('a#refresh-cw').on('click', function(){
    const checkLenOne = (t) =>{
      if (t.children()[0].querySelectorAll('td')[0].querySelectorAll('span')[0].innerHTML !== "กรุณาเลือกอุปกรณ์"){
        return true
      }else{
        return false
      }
    }
    if( $('tbody#table-body').children().length !== 1 || checkLenOne($('tbody#table-body'))){
      $('input[name="search"]').css({ 'border-stype': '', 'border-color': '' });
      delete state['select']
      delete state['option']
      checkFrom('Reset');
    }else{
      $('div#err-search').css({'overflow-y': "auto", 'display': ""});
      $('button:button#btn-err').on('click', function(){
        $('div#err-search').css('display', 'none');
      });
    }
  })
}
